
$(function () {
    $.ajaxSetup({
        headers: { "X-CSRFToken": getCookie("csrftoken") }
    });
});


$(document).ready(function() {
$("#btn-get-city").click(function() {
    var cityName = $("#city-name").val();
    $.ajax({
        type: 'POST',
        url: 'get_city_id',
        data: {'city_name': cityName},
        success: function (data) {
            var cities = JSON.parse(data);
            $("#block-result-id").empty();
            for (var city in cities){
                $("#block-result-id").append("<div class='block-city-id'><p>ID: "
                    + cities[city]["cid"] + "</p><p>" + cities[city]["title"]
                    + "</p><p>" + cities[city]["region"]
                    + "</p><p>" + cities[city]["area"] + "</p></div>"
                );
            }
            if ($('#block-result-id div').length == 0) {
                $("#block-result-id").html("<p>Нет результатов..</p>");
            }
        }
    });
    return false;
})});


$(document).ready(function() {
$("#btn-city-clear").click(function() {
    $("#block-result-id").empty();
})});


$(document).ready(function() {
$("#btn-switch-panel").click(function() {
    var cityID = $("#city-id").val();
    $.ajax({
        type: 'POST',
        url: '',
        data: {'city_id': cityID}
    });
    return false;
})});