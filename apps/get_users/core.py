import os
import threading
import time
import requests
from queue import Queue

from django.db import transaction
from requests.exceptions import ReadTimeout
from vk.exceptions import VkAPIError

if __name__ == '__main__':
    import django
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "gmproject.settings")
    django.setup()

from gmproject.settings import logger
from apps.get_users.models import UserVK, HistoryOfReceipt
from apps.support.mixins import RequestMixin, TokenMixin
from apps.support.helpers import Timer, valid_key, valid_index, vkdate_to_date
from apps.get_users.helpers import get_city_name
from apps.support.api import get_token_api


class GetUsers(RequestMixin, TokenMixin):
    """
    Получение пользователей по городу
    Параметра запуска:
        city_id - id города
        age_min - минимальный возраст для получения
        age_max - максимальный возраст для получения
        fields - список полей через запятую, которые нужно получить
    Режимы запуска:
        start('save') - с сохранением в бд.
        start('list') - возвращает список пользователей *
    * - список хранится в оперативной памяти, следует это учитыват при
        получении участников из больших городов
    """

    AGE_MIN = 10
    AGE_MAX = 116

    FIELDS = """
        sex, bdate, contacts, domain, about, relation, activities,
        interests, books, games, movies, music, tv, personal, education,
        connections, followers_count, home_town, last_seen, nickname,
        occupation, quotes, status
    """

    def __init__(self, city_id, age_min=AGE_MIN, age_max=AGE_MAX,
                 fields=FIELDS):
        super(GetUsers, self).__init__()
        self.time_total = Timer()
        self.city_id = city_id
        self.age_min = age_min
        self.age_max = age_max
        self.fields = fields
        self.received_users = []
        self.count_received = 0
        self.queue = self.create_queue()

    def create_queue(self):
        """
        Создание и наполнение очереди запросов
        """
        queue = Queue()
        ages = [(age,) for age in range(self.age_min, self.age_max + 1)]
        for age in ages:
            queue.put(age)
        return queue

    def start(self, result='save'):
        """
        Запускает процесс получения пользователей
        :param result: 'save' (по умолчанию) - сохранение в базу, 'list' -
            сохранение в оперативную память, возвращает список
        :return: Если result == 'list' - список полученных пользователей
        """
        logger.info('Start | process=GetUsers')
        while self.queue.unfinished_tasks != 0:
            if self.queue.empty() or threading.activeCount() > 450:
                continue
            time.sleep(self.TIME_OUT)
            threading.Thread(target=self.start_thread, args=[result]).start()
        self.queue.join()
        self.free_token()

        HistoryOfReceipt.objects.create(
            city_id=self.city_id,
            city_name=get_city_name(self.city_id),
            count_users=self.count_received
        )

        logger.info('Finish | process=GetUsers, total_received=%s, '
                    'time_passed=%s' %
                    (self.count_received, self.time_total.check))
        if result == 'list':
            return self.received_users

    def get_users(self, age, b_day=0, b_month=0, is_empty=0, sex=0):
        """
        Выполняет запрос users.search в vkapi
        :param age: Возраст
        :param b_day: Номер дня рождения
        :param b_month: Номер месяца рождение
        :param is_empty: Указатель, что в прошлый раз получен ноль. Здесь нужен
            только для правильной распаковки параметров
        :param sex: Пол
        :return: Список пользователей, код ошибки.
        """
        error = 0
        users = []

        try:
            # if is_empty == 2 or is_empty == 1:
            #     api = self.get_token_api(self.current_reserve)
            # else:
            #     api = self.get_token_api(self.current_token)

            api = get_token_api(self.next_token)

            users = api.users.search(
                count=1000, age_from=age, age_to=age, city=self.city_id,
                birth_day=b_day, birth_month=b_month, fields=self.FIELDS,
                sex=sex, v="5.8"
            )["items"]

        except VkAPIError as err:
            logger.error('VkAPIError | Code=%s' % err.code)
            logger.exception(err)
            error = 1

        except ReadTimeout as err:
            logger.error('ReadTimeout')
            logger.exception(err)
            error = 2

        except requests.exceptions.HTTPError as err:
            logger.error('requests.exceptions.HTTPError')
            logger.exception(err)
            error = 3

        return users, error

    def start_thread(self, result='save'):
        """
        Функция поток. Если ошибка получения - добавляет запрос в очередь;
        полученно 0 - добавляет запрос в очередь с пометкой, что было 0;
        получено > 0 и < 920 - выполняет сохранение или добавление в список;
        получено > 920 - разбивает по дням и добавляет запросы в очередь;
        получено > 920 если присутствует день - разбивает по месяцам и добавляет
            запросы в очередь;
        получено > 920 если присутствует день и месяц - разбивает по полу и
            добавляет запросы в очередь;
        :param result: Указывает, что делать с данными. 'save' - сохранение в
            бд, 'list' - добавить результат в список
        """
        queue_item = self.queue.get()
        age = queue_item[0]
        b_day = valid_index(queue_item, 1)
        b_month = valid_index(queue_item, 2)
        is_empty = valid_index(queue_item, 3)
        sex = valid_index(queue_item, 4)
        users, error = self.get_users(*queue_item)

        if error:
            self.queue.put((age, b_day, b_month, is_empty, sex))
            #self.change_token()
            self.queue.task_done()
            return

        len_users = len(users)
        if not b_day and len_users > 920:
            days = [(age, day) for day in range(1, 32)]
            for day in days:
                self.queue.put(day)
            self.queue.task_done()
            return

        if b_day and b_month and len_users > 920:
            self.queue.put((age, b_day, b_month, is_empty, 1))
            self.queue.put((age, b_day, b_month, is_empty, 2))
            self.queue.task_done()
            return

        if b_day and len_users > 920:
            months = [(age, b_day, month) for month in range(1, 13)]
            for month in months:
                self.queue.put(month)
            self.queue.task_done()
            return

        if (len_users > 0) and (len_users <= 920):
            if result == 'save':
                self.save_users(users, age, b_day, b_month, sex)
            elif result == 'list':
                self.received_users += users
            self.count_received += len_users
            self.queue.task_done()
            return

        if (is_empty == 0) and (len_users == 0):
            logger.warning(('Return 0 | First | process=GetUsers | age=%s,%s%s '
                           'time_passed=%s' % (age, ' b_month=%s,' %
                            b_month if b_month else '', ' b_day=%s,' % b_day
                            if b_day else '', self.time_total.check)))
            #self.change_token()
            self.queue.put((age, b_day, b_month, 1, sex))
            self.queue.task_done()
            return

        if (is_empty == 1) and (len_users == 0):
            logger.warning(('Return 0 | Second | process=GetUsers | age=%s,%s%s'
                           ' time_passed=%s' % (age, ' b_month=%s,' % b_month
                            if b_month else '', ' b_day=%s,' % b_day
                            if b_day else '', self.time_total.check)))
            #self.change_token()
            #self.change_reserve()
            self.queue.put((age, b_day, b_month, 2, sex))
            self.queue.task_done()
            return

        if (is_empty == 2) and (len_users == 0):
            logger.warning(('Return 0 | Third | process=GetUsers | age=%s,%s%s '
                           'time_passed=%s' % (age, ' b_month=%s,' % b_month
                            if b_month else '', ' b_day=%s,' % b_day
                            if b_day else '', self.time_total.check)))
            #self.change_reserve()
            self.queue.task_done()
            return

    @transaction.atomic
    def save_users(self, users, age, b_day, b_month, sex):
        """
        Сохранение полученных пользователей в БД
        """
        for user in users:
            UserVK.objects.create(
                user_id=user['id'],
                domain=valid_key(user, 'domain'),
                city_id=self.city_id,
                first_name=user['first_name'],
                last_name=user['last_name'],
                sex=user['sex'],
                bdate=valid_key(user, 'bdate'),
                ydate=vkdate_to_date(valid_key(user, 'bdate')),
                age_add=age,
                verified=valid_key(user, 'verified'),
                deactivated=valid_key(user, 'deactivated'),
                last_seen=valid_key(user, 'last_seen', inserted='time'),
                followers_count=valid_key(user, 'followers_count'),
                nickname=valid_key(user, 'nickname'),
                home_town=valid_key(user, 'home_town'),
                university=valid_key(user, 'university'),
                about=valid_key(user, 'about'),
                activities=valid_key(user, 'activities'),
                interests=valid_key(user, 'interests'),
                inspired_by=valid_key(
                    user, 'personal', inserted='inspired_by'),
                quotes=valid_key(user, 'quotes'),
                status=valid_key(user, 'status'),
                books=valid_key(user, 'books'),
                games=valid_key(user, 'games'),
                movies=valid_key(user, 'movies'),
                music=valid_key(user, 'music'),
                tv=valid_key(user, 'tv'),
                langs=valid_key(user, 'personal', inserted='langs'),
                relation=valid_key(user, 'relation'),
                political=valid_key(user, 'personal', inserted='political'),
                religion=valid_key(user, 'personal', inserted='religion'),
                people_main=valid_key(
                    user, 'personal', inserted='people_main'),
                life_main=valid_key(user, 'personal', inserted='life_main'),
                smoking=valid_key(user, 'personal', inserted='smoking'),
                alcohol=valid_key(user, 'personal', inserted='alcohol'),
                mobile_phone=valid_key(user, 'mobile_phone'),
                skype=valid_key(user, 'skype'),
                facebook=valid_key(user, 'facebook'),
                twitter=valid_key(user, 'twitter'),
                livejournal=valid_key(user, 'livejournal'),
                instagram=valid_key(user, 'instagram')
            )
        logger.info('Saved | process=GetUsers | age=%s,%s%s%s saved=%s' %
                    (age, ' b_month=%s,' % b_month if b_month else '',
                     ' b_day=%s,' % b_day if b_day else '',
                     ' sex=%s,' % sex if sex else '', len(users)))


class GetGroupsUser(RequestMixin, TokenMixin):
    """
    Получение топ групп пользователей по городу. Сохранение в БД.
    """

    TOP_COUNT = 10

    def __init__(self, city_id):
        super(GetGroupsUser, self).__init__()
        self.time_total = Timer()
        self.city_id = city_id
        self.users_city = UserVK.objects.filter(
            city_id=self.city_id).values_list('user_id', flat=True)
        self.users_total = len(self.users_city)
        self.queue = self.create_queue()
        self.saved_users = 0

    def create_queue(self):
        """
        Создание и наполнение очереди запросов
        """
        queue = Queue()
        group_users = self.grouping(self.users_city, 25)
        for group in group_users:
            queue.put(group)
        return queue

    def run(self):
        """
        Запускает процесс получения топ групп пользователя
        """
        logger.info('Start | process=GetUsers')
        while not self.queue.empty():
            time.sleep(self.TIME_OUT)
            th = threading.Thread(target=self.start_thread)
            th.start()
        self.queue.join()
        logger.info('Finish | process=GetUsers, total_saved=%s, time_passed=%s'
                    % (self.saved_users, self.time_total.check))

    def get_groups(self, list_users):
        """
        Получение групп 25ти пользователей методом execute
        """

        code = """
        var list_users = {0};
        var groups = [];
        var i = 0;
        while (i < 25){{
          if (list_users[i] == null){{
            return groups;
          }};
          groups.push([list_users[i], API.groups.get({{
            "user_id": list_users[i], "v": "5.62", "count": {1}}}).items]
          );
          i = i + 1;
        }};
        return groups;
        """.format(list_users, self.TOP_COUNT)

        return self.execute(code, self.current_token)

    @staticmethod
    def grouping(arr, n):
        """
        Разбивает список lst на список списков по n элементов
        """
        return [arr[i:i + n] for i in range(0, len(arr), n)]

    @transaction.atomic
    def save_groups(self, groups):
        try:
            for suite in groups:
                for group in suite[1]:
                    GroupsUserVK.objects.create(
                            user=UserVK.objects.get(user_id=suite[0]),
                            group_id=group,
                        )
            self.saved_users += len(groups)
            logger.info('Saved | process=GetUsers | saved_user=%s, total=%s' %
                        (self.saved_users, self.users_total))
        except Exception as err:
            logger.error('Save failed')
            logger.exception(err)

    def start_thread(self):
        """
        Функция поток, в зависимисти от результата get_groups, повторно
        добавляет в очередь запросы, сохраняет данные или завершает задачу
        """
        queue_item = self.queue.get()
        groups, error = self.get_groups(queue_item)
        if error:
            self.queue.put(queue_item)
            self.queue.task_done()
        else:
            self.save_groups(groups)
            self.queue.task_done()



