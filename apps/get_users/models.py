from django.db import models


class UserVK(models.Model):

    user_id = models.IntegerField()
    domain = models.CharField(max_length=40, verbose_name='Короткий адрес страницы', null=True)
    city_id = models.IntegerField()
    first_name = models.CharField(max_length=60)
    last_name = models.CharField(max_length=60)

    sex = models.IntegerField()
    bdate = models.TextField(verbose_name='Дата рождения', null=True)
    ydate = models.DateField(verbose_name='Валидная дата рождения', null=True)
    date_add = models.DateTimeField(auto_now_add=True, verbose_name='Дата добавления пользователя')
    modified = models.DateTimeField(verbose_name="Дата изменения", auto_now=True)
    age_add = models.CharField(max_length=30, verbose_name='Возраст на момент добавления')

    verified = models.IntegerField(verbose_name='Верификация', null=True)
    deactivated = models.CharField(max_length=50, verbose_name='Статус страницы', null=True)
    last_seen = models.TextField(verbose_name='Время последнего посещения (Unixtime)', null=True)
    followers_count = models.IntegerField(verbose_name='Количество подписчиков', null=True)

    nickname = models.TextField(verbose_name='Отчество (Ник)', null=True)
    home_town = models.TextField(verbose_name='Родной город', null=True)
    university = models.IntegerField(verbose_name='Идентификатор университета (первого)', null=True)

    about = models.TextField(verbose_name='О себе', null=True)
    occupation = models.CharField(max_length=50, verbose_name='Занятие', null=True)
    activities = models.TextField(verbose_name='Деятельность', null=True)
    interests = models.TextField(verbose_name='Интересы', null=True)
    inspired_by = models.TextField(verbose_name='Вдохновляет', null=True)
    quotes = models.TextField(verbose_name='Цитаты', null=True)
    status = models.TextField(verbose_name='Статус', null=True)
    books = models.TextField(verbose_name='Любимые книги', null=True)
    games = models.TextField(verbose_name='Любимые игры', null=True)
    movies = models.TextField(verbose_name='Любимые фильмы', null=True)
    music = models.TextField(verbose_name='Любимая музыка', null=True)
    tv = models.TextField(verbose_name='Любимые телешоу', null=True)

    # personal
    langs = models.TextField(verbose_name='Языки', null=True)
    relation = models.IntegerField(verbose_name='Семейное положение', null=True)
    political = models.IntegerField(verbose_name='Политические предпочтения', null=True)
    religion = models.TextField(verbose_name='Религия', null=True)
    people_main = models.IntegerField(verbose_name='Главное в людях', null=True)
    life_main = models.IntegerField(verbose_name='Главное в жизни', null=True)
    smoking = models.IntegerField(verbose_name='Отношение к курению', null=True)
    alcohol = models.IntegerField(verbose_name='Отношение к алкоголю', null=True)

    mobile_phone = models.CharField(max_length=30, null=True)
    skype = models.CharField(max_length=80, null=True)
    facebook = models.CharField(max_length=80, null=True)
    twitter = models.CharField(max_length=80, null=True)
    livejournal = models.CharField(max_length=80, null=True)
    instagram = models.CharField(max_length=80, null=True)


class HistoryOfReceipt(models.Model):

    city_id = models.IntegerField(verbose_name="ID Города")
    city_name = models.CharField(verbose_name="Название города", max_length=180)
    count_users = models.IntegerField(verbose_name="Количество пользователей")
    date_add = models.DateField(auto_now_add=True, verbose_name="Дата сбора")




# class GroupsUserVK(models.Model):
#
#     user = models.ForeignKey(UserVK)
#     group_id = models.IntegerField()
#     name = models.CharField(max_length=150)
#     date_add = models.DateField(auto_now=True)


# class MemberGroup(models.Model):
#
#     user_id = models.IntegerField()
#     group_id = models.CharField(max_length=150)
#     city_id = models.IntegerField(null=True)
#     sex = models.IntegerField(null=True)
#     bdate = models.TextField(verbose_name='Дата рождения', null=True)
#     date_add = models.DateField(auto_now=True)



