from apps.support.api import get_api


def get_city_id(city):
    api = get_api()
    cities = api.database.getCities(
        q=str(city), country_id=1, need_all=1, v="3")
    return cities


def check_city_id(id):
    api = get_api()
    city = api.database.getCitiesById(city_ids=id, v="3")
    if city[0].get("name"):
        return True
    return False


def get_city_name(id):
    api = get_api()
    city = api.database.getCitiesById(city_ids=id, v="3")
    return city[0].get("name", None)
