from django.conf.urls import url
from apps.get_users.actions import GetUsersAction, CityDeleteAction, GetCityIdAjax

urlpatterns = [
    url(r'^$', GetUsersAction.as_view(), name='get_users'),
    url(r'^get_city_id$', GetCityIdAjax.as_view(), name='get_city_id'),
    url(r"^delete/(?P<history_id>\d+)$", CityDeleteAction.as_view(),
        name="city_delete"),
]

