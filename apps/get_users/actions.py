import json

from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic.base import TemplateView, View
from django.views.generic.list import ListView
from django.urls import reverse_lazy, reverse
from django.contrib import messages
from django.shortcuts import redirect
from apps.get_users.helpers import get_city_id, check_city_id
from apps.get_users.models import HistoryOfReceipt, UserVK
from apps.get_users.core import GetUsers
from apps.support.mixins import AuthenticateMixin
from django.views.generic.edit import DeleteView


class GetUsersAction(AuthenticateMixin, ListView):
    template_name = "get_users.html"
    model = HistoryOfReceipt

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            if request.POST.get("city_id"):
                city_id = int(request.POST.get("city_id"))
                if not check_city_id(city_id):
                    messages.add_message(
                        request, messages.ERROR, "Такого города нет")
                    return HttpResponseRedirect(reverse_lazy("get_users"))
                elif not HistoryOfReceipt.objects.filter(city_id=city_id):
                    GetUsers(city_id=city_id).start()
                    messages.add_message(
                        request, messages.SUCCESS, "Сбор данных завершен")
                    return HttpResponseRedirect(reverse_lazy("get_users"))
                else:
                    messages.add_message(
                        request, messages.ERROR, "Этот город уже получен")
                    return HttpResponseRedirect(reverse_lazy("get_users"))
            return HttpResponse()


class GetCityIdAjax(AuthenticateMixin, View):

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            if request.POST.get("city_name"):
                city_name = request.POST.get("city_name")
                result = json.dumps(get_city_id(city_name))
                return HttpResponse(result)
            return HttpResponse()


class CityDeleteAction(AuthenticateMixin, DeleteView):
    model = HistoryOfReceipt
    success_url = reverse_lazy("get_users")
    pk_url_kwarg = "history_id"

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        history = HistoryOfReceipt.objects.filter(
            id=self.kwargs.get(self.pk_url_kwarg))
        if not history:
            messages.add_message(
                self.request, messages.ERROR, "Данных уже не существует")
            return redirect(reverse_lazy("get_users"))
        else:
            self.delete_users(history)
            messages.add_message(
                request, messages.SUCCESS, "Данные по городу удалены")
        return super(CityDeleteAction, self).post(request, *args, **kwargs)

    def delete_users(self, history):
        users = UserVK.objects.filter(city_id=history[0].city_id)
        if users:
            users.delete()
