import threading
import googlemaps
from queue import Queue
from django.db.utils import OperationalError
from requests.exceptions import ReadTimeout, ConnectionError
from googlemaps import geocoding
from googlemaps.exceptions import TransportError, Timeout
from vk.exceptions import VkAPIError
from gmproject.settings import logger, GOOGLE_API_KEY
from apps.support.mixins import RequestMixin
from apps.support.helpers import Timer, valid_key
from apps.support.api import get_api, get_token_api


class GetMembers(RequestMixin):
    """
    Получение участников сообщества
    Параметры запуска:
        group_id - идентификатор или домен группы
        fields - список полей через запятую, которые нужно получить
    Режимы запуска:
        start('list') - возвращает список участников *
        start_annotate(field) - получает аннотацию по указанному полю
    * - список хранится в оперативной памяти, следует это учитыват при
        получении участников из больших групп
    """

    FIELDS = "city, sex, bdate"

    def __init__(self, group_id, fields=FIELDS):
        super(GetMembers, self).__init__()
        self.time_total = Timer()
        self.group_id = group_id
        self.fields = fields
        self.api = get_api() #get_token_api("74a075d7b03bb4a02a5f18d29ada5f20c18455681a8a950a8d733bf089d51bf688f2194d472da3f66380e")
        self.members = []
        self.total_members = self.api.groups.getMembers(
            group_id=self.group_id, count=0, v="5.9"
        )['count']
        self.offset_queue = self.create_offset_queue()
        self.annotate_queue = Queue()
        self.annotate_result = {}

    @property
    def amount_received(self):
        return len(self.members)

    def create_offset_queue(self):
        """
        Создание и наполнение очереди запросов
        """
        queue = Queue()
        offset = 0
        request_count = int(self.total_members / 1000) + 1
        for i in range(0, request_count + 1):
            queue.put(offset)
            offset += 1000
        return queue

    def get_members(self, offset):
        """
        Выполняет запрос groups.getMembers в vkapi
        :param offset: Смещение
        :return: Результат запроса, код ошибки
        """
        members = []
        error = 0

        try:
            members = self.api.groups.getMembers(
                group_id=self.group_id, count=1000, offset=offset, v="5.9",
                fields=self.fields
            )['items']

        except VkAPIError as err:
            logger.error('VkAPIError | Code=%s' % err.code)
            logger.exception(err)
            error = 1

        except ReadTimeout as err:
            logger.error('ReadTimeout')
            logger.exception(err)
            error = 2

        except ConnectionError as err:
            logger.error('ConnectionError')
            #logger.exception(err)
            error = 3

        return members, error

    def start(self, result='list'):
        """
        Запускает процесс получения участников
        :param result: Указывает что делать с результатом, list - вернуть список
        :return: Результат получения
        """
        logger.info('Start | process=GetMembers')
        while self.offset_queue.unfinished_tasks != 0:
            if self.offset_queue.empty() or threading.activeCount() > 150:
                continue
            th = threading.Thread(target=self.thread, args=[result])
            th.start()
        self.offset_queue.join()
        logger.info('Finish | process=GetMembers, total_received=%s, '
                    'time_passed=%s' %
                    (self.amount_received, self.time_total.check))
        if result == 'list':
            return self.members

    def thread(self, result):
        """
        Функция поток. Если get_members вернул ошибку - повторно
        добавляет запросы в очередь. При result=annotate добавляет участников в
        очередь на обработку, иначе сохраняет данные и завершает задачу
        """
        offset = self.offset_queue.get()
        members, error = self.get_members(offset)
        if error:
            self.offset_queue.put(offset)
            self.offset_queue.task_done()
        else:
            if result == 'list':
                self.members += members
            if result == 'annotate':
                self.annotate_queue.put(members)
            self.offset_queue.task_done()
            logger.info('Received | process=GetMembers | offset=%s, total=%s' %
                        (offset, self.total_members))

    def start_annotate(self, field='city', recalc=False):
        """
        Запускает аннотацию данных по полю field. Если данные есть в бд,
        вернет оттуда.
        :param field: Поле по которому проводится аннотация
        :param recalc: Перерасчет. True - принудительный перерасчет данных.
        :return: Аннотированный словарь
        """
        if not recalc:
            annotation = self.unload_data(field)
            if annotation:
                return annotation
        logger.info('Start | process=GetMembers.annotate | field=%s' % field)
        threading.Thread(target=self.annotate, args=[field]).start()
        threads = []
        while self.offset_queue.unfinished_tasks != 0:
            if self.offset_queue.empty() or threading.activeCount() > 200:
                continue
            threads.append(threading.Thread(
                target=self.thread, args=['annotate']))
            threads[-1].start()
        self.offset_queue.join()
        logger.info('Finish | process=GetMembers.annotate | field=%s, '
                    'time_passed=%s' % (field, self.time_total.check))
        self.annotate_queue.join()
        print(self.annotate_result)
        self.save_data(field, self.annotate_result)
        for th in threads:
            th.join()
        annotation = self.unload_data(field)
        return annotation

    def annotate(self, field, inserted=''):
        """
        Выполняет аннотацию участников сообщества по полю field.
        Чтобы не потерять данные, берет их из очереди на обработку.
        """
        while (self.offset_queue.unfinished_tasks != 0 or
               self.annotate_queue.unfinished_tasks != 0):
            members = self.annotate_queue.get()
            for member in members:
                if field == 'city':
                    try:
                        self.annotate_result[
                            valid_key(member, field, inserted='id', rtn=0)
                        ]['members'] = valid_key(self.annotate_result[valid_key(
                            member, field, inserted='id', rtn=0)],
                            'members', rtn=0) + 1
                    except KeyError:
                        self.annotate_result[valid_key(
                            member, field, inserted='id', rtn=0)] = {
                            'title': valid_key(member, field, inserted='title',
                                               rtn=''), 'members': 1}
                else:
                    self.annotate_result[valid_key(
                        member, field, inserted=inserted, rtn=0)] = (
                        valid_key(self.annotate_result, valid_key(
                            member, field, inserted=inserted, rtn=0
                        ), rtn=0) + 1)
            self.annotate_queue.task_done()

    def unload_data(self, field):
        """
        Выгрузка словаря из базы.
        :param field: Поле по которому был посчет
        :return: Словарь, None - если данных нет.
        """
        data = MembersByField.objects.filter(
            domian=self.group_id, field=field).first()
        if data:
            data = data.data
        else:
            data = None
        return data

    def save_data(self, field, data):
        """
        Сохранение словаря в базу.
        :param field: Поле по которому был посчет
        :param data: Словарь
        """
        MembersByField.objects.update_or_create(
            domian=self.group_id,
            field=field,
            defaults={'data': data}
        )


class GetGeocode(RequestMixin):
    """
    Получение геокода города
    Параметры запуска:
        city_ids - список идентификаторов городов, геокод которых нужно получить
    Режимы запуска:
        start() - возвращает словарь с геокодами городов. Словарь вида -
        {'city_id': {'lat': latitude, 'lng': longitude}}
    """

    def __init__(self, city_ids):
        self.city_ids = city_ids
        self.api = self.get_api()
        self.country_ids = self.get_country_ids()
        self.geocode = {}
        self.queue_cities = self.create_queue_cities()
        self.queue_save = Queue()

    def create_queue_cities(self):
        """
        Создание очереди из id городов, для получения геокода.
        :return: Очередь
        """
        queue = Queue()
        for city_id in self.city_ids:
            queue.put(city_id)
        return queue

    def save_geocode(self):
        """
        Сохранение полученного геокода в бд
        """
        while (self.queue_save.unfinished_tasks != 0 or
               self.queue_cities.unfinished_tasks != 0):
            if self.queue_save.empty() or threading.activeCount() > 450:
                continue
            geocode = self.queue_save.get()
            try:
                Geocode.objects.update_or_create(
                    city_id=geocode[0], defaults={
                        'latitude': geocode[1],
                        'longitude': geocode[2],
                        'name': geocode[3],
                        'region': geocode[4],
                        'area': geocode[5]
                    })
            except OperationalError:
                logger.error('OperationalError')
                self.queue_save.put(geocode)
            self.queue_save.task_done()

    def start(self):
        logger.info('Start | process=GetGeocode | total=%s' %
                    self.queue_cities.unfinished_tasks)
        time_total = Timer()
        threading.Thread(target=self.save_geocode, args=[]).start()
        while self.queue_cities.unfinished_tasks != 0:
            if self.queue_cities.empty():
                continue
            threading.Thread(target=self.get_city_geocode, args=[]).start()
        self.queue_cities.join()
        logger.info('Finish | process=GetGeocode | time_passed=%s' %
                    time_total.check)
        return self.geocode

    def get_city_geocode(self):
        """
        Получение геокода из google maps api
        """
        time_received = Timer()
        city_id = self.queue_cities.get()
        title, region, area = self.get_region_city(city_id)
        place = ' '.join([title, region, area])
        latitude, longitude = self.get_google_geocode(place)
        self.queue_save.put((city_id, latitude, longitude, title, region, area))
        logger.info('Received | process=GetGeocode | city_id=%s, '
                    'geocode=%s, time_passed=%s' %
                    (city_id, (latitude, longitude), time_received.check))
        self.geocode[str(city_id)] = {'lat': latitude, 'lng': longitude}
        self.queue_cities.task_done()

    def get_region_city(self, city_id, title=None):
        """
        Получает область и район города
        :param city_id: id города
        :param title: Наименование города. Если не указано - получаем
        :return: Наименование, область и район города
        """
        if not title:
            title = self.api.database.getCitiesById(
                city_ids=city_id, v='5.62')[0]['title']
        country_ids = self.country_ids
        country_ids = country_ids[:6]
        for country_id in country_ids:
            cities = self.api.database.getCities(
                q=title[:15], country_id=country_id, v='5.62')['items']
            for city in cities:
                if city['id'] == int(city_id):
                    region = valid_key(city, 'region', rtn='')
                    area = valid_key(city, 'area', rtn='')
                    return title, region, area
        return title, '', ''

    def get_google_geocode(self, place, language='RU'):
        """
        Возвращает геокод по названию города.
        :param place: Наименование города, района, области.
        :param language: Язык
        :return: Широта и долгота города если найден, иначе None
        """
        gmaps = googlemaps.Client(key=GOOGLE_API_KEY)
        geocode = geocoding.geocode(gmaps, place, language=language)
        if geocode:
            try:
                geocode = geocode[0]['geometry']['location']
            except TransportError or Timeout:
                logger.error('TransportError | Max retries')
                return
            return geocode['lat'], geocode['lng']
        return None, None

    def get_country_ids(self):
        """
        Возвращает id всех стран во позрастанию
        :return: список id
        """
        countries = self.api.database.getCountries(
            need_all=1, count=1000, v='5.62')['items']
        country_ids = []
        for country in countries:
            country_ids.append(country['id'])
        country_ids.sort()
        return country_ids

