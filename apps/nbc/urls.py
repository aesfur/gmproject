from django.conf.urls import url
from apps.nbc.actions import TrainAction, ClassifierAction


urlpatterns = [
    url(r'^nbc/train', TrainAction.as_view(), name='nbc_train'),
    url(r'^nbc/classifier', ClassifierAction.as_view(), name='nbc_classifier')
]

