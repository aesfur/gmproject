# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2018-02-14 15:47
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Categories',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('category', models.CharField(max_length=150, verbose_name='Категория')),
                ('count_doc', models.IntegerField(verbose_name='Количество документов')),
            ],
            options={
                'verbose_name': 'Категория',
                'verbose_name_plural': 'Категории',
            },
        ),
        migrations.CreateModel(
            name='Words',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('word', models.CharField(max_length=60, verbose_name='Слово')),
                ('count_word', models.IntegerField(verbose_name='Количество слов')),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='nbc.Categories')),
            ],
            options={
                'verbose_name': 'Слово',
                'verbose_name_plural': 'Слова',
            },
        ),
    ]
