from django.urls import reverse_lazy
from django.shortcuts import redirect
from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.base import TemplateView
from apps.nbc.process import save_document, Classifier
from apps.support.mixins import AuthenticateMixin
from apps.nbc.models import Categories


class TrainAction(AuthenticateMixin, ListView):
    template_name = "nbc_train.html"
    model = Categories

    ordering = ['-category']

    def get_context_data(self, **kwargs):
        context = super(TrainAction, self).get_context_data(**kwargs)
        return context

    def post(self, request, *args, **kwargs):
        if request.POST:
            category = request.POST.get("category")
            text = request.POST.get("text")
            if category and text:
                save_document(category, text)
        return redirect(reverse_lazy("nbc_train"))


class ClassifierAction(AuthenticateMixin, TemplateView):
    template_name = "nbc_classifier.html"

    def post(self, request, *args, **kwargs):
        context = {}
        if request.POST:
            text = request.POST.get("text")
            if text:
                context["result"] = Classifier().classifier(text)
                context["text"] = text
        return render(request, self.template_name, context)



