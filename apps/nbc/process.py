import re
import nltk
import pymorphy2
import threading

from queue import Queue
from math import log, exp
from django.db.models import F, Sum
from django.db import transaction
from apps.nbc.models import Categories, Words, CategoryWords
from apps.support.helpers import Timer, sort_dct
from gmproject.settings import logger


class NBCModel:
    """
    Модель NBC
    """

    def __init__(self, category_id):
        self.categories = Categories.objects.all()
        self.category = self.categories.get(id=category_id)
        self.words = Words.objects.all()
        self.categ_words = CategoryWords.objects.filter(category=self.category)

    @property
    def dictionary_size(self):
        """
        Размер словаря обучающей выборки (количество уникальных слов)
        :return: |V|
        """
        return self.words.count()

    @property
    def sum_words(self):
        """
        Суммарное количество слов в документах категории
        :return: Lc
        """
        return self.categ_words.count()

    @property
    def total_count_doc(self):
        """
        Общее количество документов в обучающей выборке
        :return: D
        """
        return self.categories.aggregate(count=Sum("count_doc"))["count"]

    @property
    def count_doc(self):
        """
        Количество документов категории
        :return: Dc
        """
        return self.category.count_doc

    def get_count_word(self, word):
        """
        Статистика по цитируемости слова в пределах категории
        :param word:
        :return: Wic
        """
        w = self.categ_words.filter(word__word=word)
        if w.exists():
            return w.first().count_word
        else:
            return 0


def normalization(document):
    """
    Нормализация слов из входного документа
    :param document: входной документ
    :return: список нормализованных слов
    """

    # Определяем язык документа
    # lang = detect(document)
    # if lang == "en":
    #     lang = "english"
    # elif lang == "ru":
    #     lang = "russian"
    # else:
    #     lang = "russian"

    # Убираем все, кроме слов и пробелов
    #reg = re.compile('[^а-яА-Яa-zA-Z ]')
    reg = re.compile('[^а-яА-Я ]')
    document = reg.sub('', document)

    word_list = document.split(" ")

    # Получение стоп-слов
    stop_words_ru = nltk.corpus.stopwords.words("russian")
    more = ["это", "который", "такой", "также", "этот"]
    stop_words_ru.append(more)

    # Стеммер
    # stemmer = nltk.stem.SnowballStemmer(lang)
    # stemmer.stem(word)

    # Начальная форма слова
    morph = pymorphy2.MorphAnalyzer()

    # Удаление стоп-слов и нормализация
    return [morph.parse(word)[0].normal_form for word in word_list
            if word.lower() not in stop_words_ru and len(word) > 2]


@transaction.atomic
def save_document(category_id, document):
    """
    Сохранение слов в БД
    :param category_id: id категории
    :param document: документ
    :return:
    """

    t = Timer()
    t.run()

    logger.info("Saving a document | Start")

    # Увеличиваем число обработанных документов у категории
    category = Categories.objects.get(id=category_id)
    category.count_doc += 1
    category.save()

    words = normalization(document)

    # Считаем частоту появления слова
    freq_words = {}
    for word in words:
        try:
            freq_words[word] += 1
        except KeyError:
            freq_words[word] = 1

    # Сохраняем данные
    for word in freq_words:
        # Если слово встречается меньше 5 раз - пропускаем
        if freq_words[word] <= 5:
            continue
        obj_w, created = Words.objects.get_or_create(word=word)
        obj_cw, created = CategoryWords.objects.get_or_create(
            word=obj_w, category=category,
            defaults={"count_word": freq_words[word]}
        )
        if not created:
            obj_cw.count_word = F("count_word") + freq_words[word]
            obj_cw.save()

    logger.info("Saving a document | Finish, Прошло: %s" % t.check)


class Classifier:

    def __init__(self):
        self.result = {}

    def classifier(self, document):
        """
        Классификатор
        :param document: документ на проверку
        :return:
        """

        t = Timer()
        t.run()

        logger.info("Classification | Start")

        categories = Categories.objects.filter(count_doc__gt=0)
        self.words = normalization(document)

        doc_length = len(self.words)
        logger.info("Classification | Всего слов: %s" % doc_length)

        # Считаем частоту появления слова
        freq_words = {}
        for word in self.words:
            try:
                freq_words[word] += 1
            except KeyError:
                freq_words[word] = 1

        if doc_length <= 200:
            limit = 0
        elif 200 < doc_length <= 1000:
            limit = 2
        elif 1000 < doc_length <= 3000:
            limit = 3
        else:
            limit = 5

        min_words = []

        top_words_list = []
        for item in freq_words:
            if freq_words[item] <= limit:
                min_words.append(item)
                continue
            top_words_list.append(item)

        logger.info("Classification | Топ слов: %s" % len(top_words_list))

        # Исключаем минимальные
        self.words = [i for i in self.words if i not in min_words]
        logger.info("Classification | Слов в обработке: %s" % len(self.words))

        self.queue = Queue()
        for category in categories:
            self.queue.put(category)
            threading.Thread(target=self.process_category).start()
        self.queue.join()

        logger.info("Classification | Finish, Прошло: %s" % t.check)
        logger.info("Classification | Result: %s" % self.result)

        return value_to_percent(self.result)

    def process_category(self):

        category = self.queue.get()

        logger.info("Classification | Проверка: %s" % category)
        nbc_model = NBCModel(category.id)
        self.result[category.category] = log(
            nbc_model.count_doc / nbc_model.total_count_doc) + sum(
            [log((nbc_model.get_count_word(word) + 1) / (
                    nbc_model.dictionary_size + nbc_model.sum_words)
                 ) for word in self.words])

        logger.info("Classification | Завершен: %s" % category)
        self.queue.task_done()
        return


def value_to_percent(result, limit=3):
    """
    Переводит логарифмические значения в процентную форму
    :param result: словарь с результатом
    :param limit: ограничение на самые вероятные
    :return: топ подходящих категорий
    """
    percentage = {}
    for category in result:
        current = result[category]
        try:
            percent = 1 / (1 + sum([exp(value - current) for value in
                                    result.values() if value != result[category]]))
        except OverflowError:
            percent = 0
        percentage[category] = round(percent * 100)

    logger.info("Percentage | %s" % percentage)

    tuple_p = sort_dct(percentage)[:limit]
    result = {}
    for item in tuple_p:
        if item[1] !=0:
            result[item[0]] = item[1]

    logger.info("TOP Percentage | %s" % result)

    return result
