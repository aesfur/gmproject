from django.contrib import admin
from apps.nbc.models import Categories


class NBCAdmin(admin.ModelAdmin):

    list_display = ["category", "count_doc"]
    search_fields = ["category"]
    list_display_links = ["category"]
    exclude = ["count_doc"]


admin.site.register(Categories, NBCAdmin)

