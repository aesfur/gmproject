from django.db import models


class Categories(models.Model):
    """
    Категории
    """

    category = models.CharField(max_length=150, verbose_name="Категория")
    count_doc = models.IntegerField(verbose_name="Количество документов", default=0)

    def __str__(self):
        return self.category

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"


class Words(models.Model):
    """
    Слова
    """

    word = models.CharField(max_length=150, verbose_name="Слово")
    category = models.ManyToManyField(Categories, through='CategoryWords')

    def __str__(self):
        return self.word

    class Meta:
        verbose_name = "Слово"
        verbose_name_plural = "Слова"


class CategoryWords(models.Model):
    """
    Соотношение слов и рубрики
    """

    word = models.ForeignKey(Words, on_delete=False)
    category = models.ForeignKey(Categories, on_delete=False)
    count_word = models.IntegerField(verbose_name="Количество слов")
