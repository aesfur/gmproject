import os

from datetime import date
from gmproject.settings import DATA_DIR


def load_lines(filename):
    path = os.path.join(DATA_DIR, filename)
    with open(path, 'r', encoding='utf-8') as file:
        for line in file:
            line = line.rstrip('\n')
            yield line


def get_date_by_age(age):
    d = date.today()
    year = d.year - int(age)
    d = d.replace(year=year)
    return d
