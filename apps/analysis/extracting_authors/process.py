import re

from yargy import Parser

from apps.analysis.extracting_authors.helpers import get_date_by_age
from apps.analysis.extracting_authors.rules import NAME
from apps.support.helpers import Timer
from apps.get_users.models import UserVK
from apps.analysis.enums import SexEnum
from gmproject.settings import logger


# Шаг извлечения
STEP = 2500


def get_data(sex=None, age_from=None, age_to=None, cities=None):

    if cities:
        books = UserVK.objects.filter(
            books__isnull=False, city_id__in=cities).exclude(
            books="").order_by("id")
    else:
        books = UserVK.objects.filter(books__isnull=False).exclude(
            books="").order_by("id")

    if age_from:
        books = books.filter(ydate__lte=get_date_by_age(age_from))
    if age_to:
        books = books.filter(ydate__gte=get_date_by_age(age_to))

    if sex == SexEnum.MALE.value:
        books = books.filter(sex=SexEnum.MALE.value)
    elif sex == SexEnum.FEMALE.value:
        books = books.filter(sex=SexEnum.FEMALE.value)

    books = books.values_list("books", flat=True)
    return books


def extraction_names(text):
    parser = Parser(NAME)
    return parser.findall(text)


def remove_unnecessary(text):
    reg = re.compile('[^а-яА-Я ,;.]')
    return reg.sub('', text)


def sort_writers_data(data):
    result = []
    for key in data:
        if key is None:
            continue
        result.append([key.title(), data[key]])
    n = 1
    while n < len(result):
        for i in range(len(result) - n):
            if (result[i][1]["male"] + result[i][1]["female"]) < (
                    result[i + 1][1]["male"] + result[i + 1][1]["female"]):
                result[i], result[i + 1] = result[i + 1], result[i]
        n += 1
    return result


def get_top_writers(data):
    t = Timer()
    t.run()
    count = len(data)
    i = 0
    j = STEP
    result = {}
    while i < count:
        logger.info('Extracting writers | %s из %s, Прошло: %s' %
                    (i, count, t.check))
        part_data = data[i:j]
        n_data = remove_unnecessary(" ".join(part_data))
        matches = extraction_names(n_data)
        for match in matches:
            try:
                word = normalize(match.fact.last.lower())
                if word:
                    result[word] += 1
            except KeyError:
                result[match.fact.last.lower()] = 1
            except TypeError:
                continue
        i += STEP
        j += STEP
    logger.info('Extracting writers | Затрачено: %s' % t.check)
    return result


def normalize(word):
    exclude = ["поттер", "холмс", "винчи", "каренина", "крузо", "роберт",
               "дейл", "маргарита", "кристо", "нарнии", "эрагон", "потер",
               "мария", "а", "по", "колец", "владимирович"]
    morph = {"толкиен": "толкин", "толстого": "толстой",
             "достоевского": "достоевский", "толкина": "толкин",
             "булгакова": "булгаков", "пушкина": "пушкин",
             "есенина": "есенин", "чехова": "чехова", "кинга": "кинг"}

    if word in exclude:
        return None
    elif word in morph.keys():
        return morph[word]
    else:
        return word


def start_extracting(age_from=None, age_to=None, cities=None):
    t = Timer()
    t.run()
    logger.info('Start Extracting')
    data_male = get_data(
        sex=SexEnum.MALE.value, age_from=age_from,
        age_to=age_to, cities=cities)
    data_female = get_data(
        sex=SexEnum.FEMALE.value, age_from=age_from,
        age_to=age_to, cities=cities)
    data_male = get_top_writers(data_male)
    data_female = get_top_writers(data_female)
    result = {}
    for m in data_male:
        result[m] = {"male": data_male[m], "female": 0}
        for f in data_female:
            try:
                result[f]["female"] = data_female[f]
            except KeyError:
                result[f] = {"male": 0, "female": data_female[f]}
    logger.info('Finish Extracting | Затрачено: %s' % t.check)
    return sort_writers_data(result)
