from yargy import rule, and_, or_, fact
from yargy.predicates import length_eq, dictionary, is_capitalized
from apps.analysis.extracting_authors.helpers import load_lines


FIRST_DICT = set(load_lines('first.txt'))
LAST_DICT = set(load_lines('last.txt'))

Name = fact(
    'Name',
    ['first', 'last']
)

#  COMPONENTS #

IS_FIRST = dictionary(FIRST_DICT)
IS_LAST = dictionary(LAST_DICT)

TITLE_FIRST_ABBR = and_(
    length_eq(1),
    is_capitalized()
)


#  ABBR #

ABBR_LAST = rule(
    TITLE_FIRST_ABBR,
    '.',
    TITLE_FIRST_ABBR,
    '.',
    is_capitalized().interpretation(
        Name.last
    )
)

ONE_ABBR_LAST = rule(
    TITLE_FIRST_ABBR,
    '.',
    is_capitalized().interpretation(
        Name.last
    )
)

#  LAST #

FIRST_LAST = rule(
    IS_FIRST,
    is_capitalized().interpretation(
        Name.last
    )
)

#  SINGLE #

JUST_LAST = rule(
    IS_LAST.interpretation(
        Name.last
    )
)

# FINAL #

NAME_ = or_(
    FIRST_LAST,
    ABBR_LAST,
    ONE_ABBR_LAST,
    JUST_LAST,
)

NAME = NAME_.interpretation(
    Name
)


