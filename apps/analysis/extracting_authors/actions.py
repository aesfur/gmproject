import json

from django.http import HttpResponse
from django.views.generic.list import ListView
from apps.analysis.extracting_authors.process import start_extracting
from apps.support.mixins import AuthenticateMixin
from apps.get_users.models import HistoryOfReceipt


class WritersAction(AuthenticateMixin, ListView):
    template_name = "writers.html"
    model = HistoryOfReceipt

    def get_context_data(self, **kwargs):
        context = super(WritersAction, self).get_context_data(**kwargs)
        return context

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            age_from = request.POST.get("age_from")
            age_to = request.POST.get("age_to")
            cities = json.loads(request.POST.get("cities"))
            result = json.dumps(start_extracting(
                age_from=age_from, age_to=age_to, cities=cities)[:60])
            return HttpResponse(result)
