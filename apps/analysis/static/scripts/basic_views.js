
$(function () {
    $.ajaxSetup({
        headers: { "X-CSRFToken": getCookie("csrftoken") }
    });
});


$(document).ready(function() {
$("#btn-switch-panel").click(function() {
    var ageFrom = $("#age-from").val();
    var ageTo = $("#age-to").val();
    var sex = $('input[type=radio]:checked').val();
    var cities = $('#select-cities').val();
    $.ajax({
        type: 'POST',
        url: '',
        data: {'age_from': ageFrom, 'age_to': ageTo,
          'sex': sex, 'cities': JSON.stringify(cities)},
        success: function (data) {
            $("#container").empty();
            startChart(data);
            if ($('#container div').length == 0) {
                $("#container").html("<p>Нет данных..</p>");
            }
        }
    });
    return false;
})});


function startChart(context) {

    var annotated_fields = JSON.parse(context);

    for (var i = 0; i < annotated_fields.length; i++ ){
        var items = annotated_fields[i]['items'];
        if (!items.length) {
            continue
        }
        var title = annotated_fields[i]['verbose'];
        var chartID = 'chart' + i;
        $('#container').append('<div class="chart" id=' + chartID + '></div>');
        getChart(craftData(items), chartID, title)
    }
}

function craftData(items) {

    var data = [];
    for (var item in items) {
        var elem = {};
        elem.name = items[item].name;
        elem.y = items[item].count;
        data.push(elem)
    }
    return data
}

function getChart(data, chartID, title) {

    $(document).ready(function () {

        getHighchartsOptions();

        Highcharts.chart(chartID, {

            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },

            title: {
                text: title
            },

            tooltip: {
                pointFormat: 'Всего: <b>{point.y}</b> <br> <b>{point.percentage:.1f} %</b>'
            },

            legend: {
                align: 'left',
                verticalAlign: 'top',
                layout: 'vertical',
                x: 0,
                y: 100
            },

            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '{point.percentage:.1f} %'
                    },
                    showInLegend: true
                }
            },

            series: [{
                colorByPoint: true,
                data: data
            }]
        });
    });
}