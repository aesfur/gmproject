
$(function () {
    $.ajaxSetup({
        headers: { "X-CSRFToken": getCookie("csrftoken") }
    });
});


$(document).ready(function() {
$("#btn-writers").click(function() {
    var ageFrom = $("#age-from").val();
    var ageTo = $("#age-to").val();
    var cities = $('#select-cities').val();
    $.ajax({
        type: 'POST',
        url: '',
        data: {'age_from': ageFrom, 'age_to': ageTo,
            'cities': JSON.stringify(cities)},
        success: function (context) {
            $("#container-writers").empty();
            var data = JSON.parse(context);
            var writers = [];
            var valuesMale = [];
            var valuesFemale = [];
            for (var item in data) {
                writers.push(data[item][0]);
                valuesMale.push(data[item][1].male);
                valuesFemale.push(data[item][1].female);
            }
            if (data.length != 0){
                startChart(writers, valuesMale, valuesFemale)
            } else {
                $("#container-writers").html("<p>Нет данных..</p>");
            }
        }
    });
    return false;
})});


function startChart(writers, valuesMale, valuesFemale) {

    getHighchartsOptions();

    Highcharts.chart('container-writers', {
        chart: {
            type: 'bar',
            height: 2000
        },
        title: {
            text: 'Популярные писатели'
        },
        xAxis: {
            categories: writers
        },
        yAxis: [{
            min: 0,
            title: {
                text: 'Количество упоминаний'
            }
        },{
            min: 0,
            title: null,
            opposite: true,
            linkedTo: 0
        }],
        legend: {
            reversed: true,
            verticalAlign: 'top'
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: [{
            name: 'Женщины',
            data: valuesFemale
        },{
            name: 'Мужчины',
            data: valuesMale
        }]
    });
}