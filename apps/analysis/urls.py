from django.conf.urls import url
from apps.analysis.basic_views.actions import AnnotateAction
from apps.analysis.extracting_authors.actions import WritersAction

urlpatterns = [
    url(r'^analysis/general$', AnnotateAction.as_view(), name='analysis_general'),
    url(r'^analysis/writers', WritersAction.as_view(), name='analysis_writers'),
]

