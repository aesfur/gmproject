from enumfields import Enum


class EnumMixin(Enum):

    @classmethod
    def get_verbose_by_value(cls, value):
        fields = cls.choices()
        for field in fields:
            if field[0] == value:
                return field[1]
        return None


class SexEnum(EnumMixin):
    """
        Пол пользователя
    """

    FIELD = 'sex'

    FEMALE = 1
    MALE = 2

    class Labels:
        FIELD = "Пол"
        FEMALE = "Женский"
        MALE = "Мужской"


class RelationEnum(EnumMixin):
    u"""
        Семейное положение пользователя.
    """

    FIELD = 'relation'

    SINGLE = 1
    FRIEND = 2
    ENGAGED = 3
    MARRIED = 4
    COMPLICATED = 5
    ACTIVE_SEARCH = 6
    LOVE = 7
    CIVIL_MARRIAGE = 8
    NOT = 0

    class Labels:
        FIELD = 'Семейное положение пользователя'
        SINGLE = 'Не женат/не замужем'
        FRIEND = 'Есть друг/есть подруга'
        ENGAGED = 'Помолвлен/помолвлена'
        MARRIED = 'Женат/замужем'
        COMPLICATED = 'Всё сложно'
        ACTIVE_SEARCH = 'В активном поиске'
        LOVE = 'Влюблён/влюблена'
        CIVIL_MARRIAGE = 'В гражданском браке'
        NOT = 'Не указано'


class PoliticalEnum(EnumMixin):
    u"""
        Политические предпочтения.
    """

    FIELD = 'political'

    COMMUN = 1
    SOCIAL = 2
    MODER = 3
    LIBERAL = 4
    CONSERVAT = 5
    MONARCH = 6
    ULTRACONSERVAT = 7
    APATHET = 8
    LIBERT = 9

    class Labels:
        FIELD = 'Политические предпочтения'
        COMMUN = "Коммунистические"
        SOCIAL = "Социалистические"
        MODER = "Умеренные"
        LIBERAL = 'Либеральные'
        CONSERVAT = 'Консервативные'
        MONARCH = 'Монархические'
        ULTRACONSERVAT = 'Ультраконсервативные'
        APATHET = 'Индифферентные'
        LIBERT = 'Либертарианские'


class PeopleMain(EnumMixin):
    u"""
        Главное в людях.
    """

    FIELD = 'people_main'

    MIND = 1
    GOODNESS = 2
    BEAUTY = 3
    AUTHORITY = 4
    COURAGE = 5
    HUMOR = 6

    class Labels:
        FIELD = 'Главное в людях'
        MIND = 'Ум и креативность'
        GOODNESS = 'Доброта и честность'
        BEAUTY = 'Красота и здоровье'
        AUTHORITY = 'Власть и богатство'
        COURAGE = 'Смелость и упорство'
        HUMOR = 'Юмор и жизнелюбие'


class LifeMainEnums(EnumMixin):
    u"""
        Главное в жизни.
    """

    FIELD = 'life_main'

    FAMILY = 1
    CAREER = 2
    ENTERTAINMENT = 3
    SCIENCE = 4
    PERFECTION = 5
    SELF_DEVELOPMENT = 6
    ART = 7
    FAME = 8

    class Labels:
        FIELD = 'Главное в жизни'
        FAMILY = 'Семья и дети'
        CAREER = 'Карьера и деньги'
        ENTERTAINMENT = 'Развлечения и отдых'
        SCIENCE = 'Наука и исследования'
        PERFECTION = 'Совершенствование мира'
        SELF_DEVELOPMENT = 'Саморазвитие'
        ART = 'Красота и искусство'
        FAME = 'Слава и влияние'


class SmokingEnum(EnumMixin):
    u"""
        Отношение к курению.
    """

    FIELD = 'smoking'

    SHARPLY_NEGATIVE = 1
    NEGATIVE = 2
    COMPROMISE = 3
    NEUTRAL = 4
    POSITIVE = 5

    class Labels:
        FIELD = 'Отношение к курению'
        SHARPLY_NEGATIVE = 'Резко негативное'
        NEGATIVE = 'Негативное'
        COMPROMISE = 'Компромиссное'
        NEUTRAL = 'Нейтральное'
        POSITIVE = 'Положительное'


class AlcoholEnum(EnumMixin):
    u"""
        Отношение к алкоголю.
    """

    FIELD = 'alcohol'

    SHARPLY_NEGATIVE = 1
    NEGATIVE = 2
    COMPROMISE = 3
    NEUTRAL = 4
    POSITIVE = 5

    class Labels:
        FIELD = 'Отношение к алкоголю'
        SHARPLY_NEGATIVE = 'Резко негативное'
        NEGATIVE = 'Негативное'
        COMPROMISE = 'Компромиссное'
        NEUTRAL = 'Нейтральное'
        POSITIVE = 'Положительное'
