from django.db.models import Count, Sum

from apps.analysis.extracting_authors.helpers import get_date_by_age
from apps.support.helpers import Timer
from apps.get_users.models import UserVK

from apps.analysis.enums import (
    RelationEnum, PeopleMain, PoliticalEnum,
    LifeMainEnums, SmokingEnum, AlcoholEnum, SexEnum
)
from gmproject.settings import logger


def get_annotated_data(age_from=None, age_to=None, sex=None, cities=None):

    logger.info('Start Analysis of basic data')

    total_time = Timer()
    CHECK_FIELDS = [
        RelationEnum, PeopleMain, PoliticalEnum, LifeMainEnums,
        SmokingEnum, AlcoholEnum
    ]

    if cities:
        users = UserVK.objects.filter(city_id__in=cities)
    else:
        users = UserVK.objects.all()

    if age_from:
        users = users.filter(ydate__lte=get_date_by_age(age_from))
    if age_to:
        users = users.filter(ydate__gte=get_date_by_age(age_to))
    if sex:
        if sex == "male":
            sex = SexEnum.MALE.value
        elif sex == "female":
            sex = SexEnum.FEMALE.value
        users = users.filter(sex=sex)

    f_result = []
    for field in CHECK_FIELDS:
        query = eval('users.filter(%s__isnull=False)' % field.FIELD.value)

        annotated_query = query.values(field.FIELD.value).annotate(
            count=Count(field.FIELD.value)).order_by(('-count'))
        total_count = annotated_query.aggregate(sum=Sum('count'))['sum']

        result = {
            'field': field.FIELD.value,
            'verbose': str(field.FIELD),
            'items': []
        }

        for obj in annotated_query:
            item = dict()
            item['name'] = field.get_verbose_by_value(
                obj[field.FIELD.value])
            if item['name'] is None:
                continue
            item['percent'] = round((obj['count'] / total_count) * 100, 1)
            item['count'] = obj['count']
            result['items'].append(item)
        print(result)
        f_result.append(result)

    logger.info('Finish Analysis of basic data | Затрачено: %s' %
                total_time.check)

    return f_result

