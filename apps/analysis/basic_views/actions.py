import json

from django.http import HttpResponse
from django.views.generic.list import ListView
from apps.analysis.basic_views.process import get_annotated_data
from apps.get_users.models import HistoryOfReceipt
from apps.support.mixins import AuthenticateMixin


class AnnotateAction(AuthenticateMixin, ListView):
    template_name = "basic_views.html"
    model = HistoryOfReceipt

    def get_context_data(self, **kwargs):
        context = super(AnnotateAction, self).get_context_data(**kwargs)
        return context

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            age_from = request.POST.get("age_from")
            age_to = request.POST.get("age_to")
            sex = request.POST.get("sex")
            cities = json.loads(request.POST.get("cities"))
            result = json.dumps(
                get_annotated_data(age_from=age_from, age_to=age_to,
                                   sex=sex, cities=cities))
            return HttpResponse(result)



