from django.conf.urls import url
from apps.authenticate.actions import LoginFormView, LogoutView

urlpatterns = [
    url(r'^login$', LoginFormView.as_view(), name='login'),
    url(r'^logout$', LogoutView.as_view(), name='logout'),
]
