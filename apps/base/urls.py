from django.conf.urls import url
from apps.base.actions import IndexAction

urlpatterns = [
    url(r'^$', IndexAction.as_view(), name='index'),
]

