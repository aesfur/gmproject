$(document).ready(function() {
    $(document).ajaxComplete(function(e, xhr, settings) {
        if (xhr.status == 278) {
            window.location.href = xhr.getResponseHeader("Location");
        }
    });
});


function getCookie(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie !== '') {
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
      var cookie = jQuery.trim(cookies[i]);
      if (cookie.substring(0, name.length + 1) === (name + '=')) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}

$.ajaxSetup({
  beforeSend: function(xhr, settings) {
    if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
      xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
    }
  }
});


$('.message-list').delay(5000).fadeOut(800);


function getHighchartsOptions() {
    return Highcharts.setOptions({
    lang: {
        contextButtonTitle: "Открыть меню",
        decimalPoint: ".",
        downloadJPEG: "Скачать JPEG images",
        downloadPDF: "Скачать PDF document",
        downloadPNG: "Скачать PNG images",
        downloadSVG: "Скачать SVG vector images",
        drillUpText: "Back to {series.name}",
        loading: "Загрузка...",
        months: [ "January" , "February" , "March" , "April" , "May" , "June" , "July" , "August" , "September" , "October" , "November" , "December"],
        noData: "Нет данных",
        numericSymbolMagnitude: 1000,
        numericSymbols: [ "k" , "M" , "G" , "T" , "P" , "E"],
        printChart: "Печать",
        resetZoom: "Reset zoom",
        resetZoomTitle: "Reset zoom level 1:1",
        shortMonths: [ "Jan" , "Feb" , "Mar" , "Apr" , "May" , "Jun" , "Jul" , "Aug" , "Sep" , "Oct" , "Nov" , "Dec"],
        shortWeekdays: undefined,
        thousandsSep: " ",
        weekdays: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
    }
    });
}