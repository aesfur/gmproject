import datetime
import webbrowser
from datetime import date

import requests

if __name__ == '__main__':
    import os
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "gmproject.settings")
    import django
    django.setup()

from apps.analysis.extracting_authors.process import extraction_names
from gmproject.settings import BASE_DIR

from apps.support.api import get_api, get_token_api, get_auth_params
from apps.get_users.models import UserVK
from apps.support.helpers import sort_dct
from apps.get_users.core import GetUsers
from apps.nbc.models import Categories, Words, CategoryWords

from apps.analysis.enums import RelationEnum, EnumMixin, SexEnum



def bdate_to_valid():
    users = UserVK.objects.all()
    for user in users:
        if user.bdate and len(user.bdate) >= 8:
            user.ydate = datetime.datetime.strptime(user.bdate, "%d.%m.%Y").date()
            user.save()


def get_date_by_age(age):
    d = date.today()
    year = d.year - int(age)
    d = d.replace(year=year)
    return d


def check_city_id(id):
    api = get_api()
    city = api.database.getCitiesById(city_ids=id)
    if city[0].get("name"):
        return True
    return False

def distance(a, b):
    "Calculates the Levenshtein distance between a and b."
    n, m = len(a), len(b)
    if n > m:
        # Make sure n <= m, to use O(min(n,m)) space
        a, b = b, a
        n, m = m, n

    current_row = range(n+1) # Keep current and previous row, not entire matrix
    for i in range(1, m+1):
        previous_row, current_row = current_row, [i]+[0]*n
        for j in range(1,n+1):
            add, delete, change = previous_row[j]+1, current_row[j-1]+1, previous_row[j-1]
            if a[j-1] != b[i-1]:
                change += 1
            current_row[j] = min(add, delete, change)

    return current_row[n]

#print(distance("Паврот", "Паврот"))


def delete_punctuation(text):
    regex = re.compile('[%s]' % re.escape(string.punctuation))
    return regex.sub(' ', text)


def is_number(word):
    u"""
    Проверка слова на число
    """
    try:
        float(word)
        return True
    except ValueError:
        return False



import math as m

# lat1 = 55.7531
# lon1 = 37.5876
# lat2 = 55.753083
# lon2 = 37.587614
# precision = 1

# distance=239.92234835934104::BDLatHome=56.7905::BDLongHome=60.5221::BDLatWork=55.753083::BDLongWork=37.587614::YandLat=56.790023::YandLong=60.525942

lat1 = 55.753083  # lat1 - Значение параметра <SUBS_LATITUDE_HOME>
lon1 = 37.587614  # lon1 - Значение параметра <SUBS_LONGITUDE_HOME>
lat2 = 56.790023  # второе значение от яндекса
lon2 = 60.525942  # первое значение от яндекса
precision = 1


def go(lat1, lon1, lat2, lon2, precision):
    a1 = lat1 * m.pi / 180
    b1 = lon1 * m.pi / 180
    a2 = lat2 * m.pi / 180
    b2 = lon2 * m.pi / 180
    print("a1:", a1, "a2:", a2, "b1:", b1, "b2:", b2)
    d = 6371009 * m.acos(m.sin(
        a1) * m.sin(a2) + m.cos(a1) * m.cos(a2) * m.cos(b1 - b2))
    print("d:", d)
    if d > precision:
        return 0
    else:
        return 1

#print("Результат:", go(lat1, lon1, lat2, lon2, precision))



import nltk
from nltk.corpus import stopwords
stop = stopwords.words('russian')

# def get_data():
#     result = {}
#
#     books = UserVK.objects.filter(books__isnull=False).exclude(books="")[:5000]
#     books = books.values("books")
#     s = ""
#     all = []
#     for book in books:
#         books_list = book["books"].replace(",", ";").replace("\"", "")
#         books = books_list.split(";")
#         all += books
#     for word in all:
#         word = word.strip()
#         s += word
#         try:
#             result[word.lower()] += 1
#         except KeyError:
#             result[word.lower()] = 1
#     print(sort_dct(result))
#
#     return result, s


# https://www.goodreads.com/api/author_url/%D0%A8%D0%B0%D0%BD%D0%BE%D0%B2?key=menDUcfbDafBasgDWkn3A
# https://www.goodreads.com/search/index.xml?key=menDUcfbDafBasgDWkn3A&q=


#############################
#from natasha import NamesExtractor, PersonExtractor
import nltk
import re
import string
from nltk.corpus import stopwords
from apps.support.helpers import Timer


def is_number(word):
    u"""
    Проверка слова на число
    """
    try:
        float(word)
        return True
    except ValueError:
        return False


def delete_punctuation(text):
    regex = re.compile('[%s]' % re.escape(string.punctuation))
    return regex.sub(' ', text)


def normalized_words(text):
    u"""
    Нормализация текста
    :param article_text: Текст
    :return: Список нормализованных слов
    """
    STOP_WORDS = stopwords.words('russian')
    words = []
    oneline = text.replace('\n', ' ')
    oneline = delete_punctuation(oneline)
    list_words = oneline.split()
    for t1 in list_words:
        t2 = t1.strip().lower()
        if t2 in STOP_WORDS or is_number(t2):
            pass
        else:
            words.append(t1)
    return words


def get_data():
    books = UserVK.objects.filter(books__isnull=False).exclude(
        books="").order_by("id").values_list("books", flat=True)
    return books



# t = Timer()
# t.run()
# data = get_data()
# count = len(data)
# i = 0
# j = 2500
# result = {}
# while i < count:
#     print("i",i," из ",count)
#     part_data = data[i:j]
#     n_data = normalized_words(" ".join(part_data))
#     matches = extraction_names(" ".join(n_data))
#     for match in matches:
#         try:
#             result[match.fact.last] += 1
#         except KeyError:
#             result[match.fact.last] = 1
#     i += 2500
#     j += 2500
#
# print(sort_dct(result))
# print(t.check)



def sort_writers_data(data):
    result = []
    for key in data:
        if key is None:
            continue
        result.append([key, data[key]])
    n = 1
    while n < len(result):
        for i in range(len(result)-n):
            if result[i][1]["male"] + result[i][1]["female"] < result[i+1][1]["male"] + result[i+1][1]["female"]:
                result[i], result[i+1] = result[i+1], result[i]
        n += 1
    return result

def count_substrings(string, substring):
    substring_re = '(?=(%s))' % re.escape(substring)
    return len(re.findall(substring_re, string))

# f = open('text.txt', 'r', encoding='utf-8')
# t = f.read()
#
# result = {}
# for i in t.lower().split():
#     try:
#         result[i] += 1
#     except KeyError:
#         result[i] = 1
#
# print(sort_dct(result))

#print(count_substrings(t.lower(), ' чехов'))


# final = {}
# f_last = open('last.txt', 'r', encoding='utf-8')
# last = f_last.readlines()
# for i in last:
#     count = count_substrings(t.lower(), ' %s ' % i)
#     if count:
#         final[i] = count
#
# print(sort_dct(final))

# k = extraction_names("чехов")
# print([i.fact for i in k])

# f = open('text.txt', 'a', encoding='utf-8')
# f.write(" ".join(n_data))




# Обучающий алгоритм классификации

# class NaiveBayesLearningAlgorithm:
#
#     examples = []
#     tokenize = v.split(' ')
#     tokenizeTuple = tokenize(v._1)
#     calculateWords = l.map(tokenizeTuple(_).length).reduceLeft(_ + _)
#
#     def addExample(ex: String, cl: String):
#         examples = (ex, cl) :: examples
#
#     def dictionary = examples.map(tokenizeTuple).flatten.toSet
#
#     def model = {
#         val docsByClass = examples.groupBy(_._2)
#         val lengths = docsByClass.mapValues(calculateWords)
#         val docCounts = docsByClass.mapValues(_.length)
#         val wordsCount = docsByClass.mapValues(_.map(tokenizeTuple).flatten.groupBy(x => x).mapValues(_.length))
#
#         new NaiveBayesModel(lengths, docCounts, wordsCount, dictionary.size)
#     }
#
#     def classifier = new NaiveBayesClassifier(model)



# Алгоритм непосредственно классификации
# @param m статистическая модель классификатора

# class NaiveBayesClassifier(m: NaiveBayesModel) {
#
#     def classify(s: String) = {
#         m.classes.toList.map(c => (c, calculateProbability(c, s))).sortBy(_._2).last._1
#     }
#
#     def tokenize(s: String) = s.split(' ')
#
#     /**
#      * Рассчитывает оценку вероятности документа в пределах класса
#      * @param c класс
#      * @param s текст документа
#      * @return оценка <code>P(c|d)</code>
#      */
#     def calculateProbability(c: String, s: String) = {
#         tokenize(s).map(m.wordLogProbability(c, _)).reduceLeft(_ + _) + m.classLogProbability(c)
#     }
# }



# /**
#  * Модель классификатора. Включает в себя всю необходимую для классификации статистику
#  *
#  * @param lengths суммарные длина документов в словах по классам      lengths: Map[String, Int],
#  * @param docCount количество документов по класам  Map[String, Int],
#  * @param wordCount статистика по цитируемости слов в пределах классов Map[String, Map[String, Int]],
#  * @param dictionarySize размер словаря обуающей выборки Int
#  */
import math
#
# class NaiveBayesModel:
#
#     def __init__(self, lengths, doc_count, word_count, dictionary_size):
#         self.lengths = lengths
#         self.doc_count = doc_count
#         self.word_count = word_count
#         self.dictionary_size = dictionary_size
#
#     # /**
#     #  * @param c класс
#     #  * @param w слово
#     #  * @return логарифм оценки P(w|c) — вероятности слова в пределах класса
#     #  */
#     def wordLogProbability(self, c,w):
#         math.log((wordCount(c).getOrElse(w, 0) + 1.0) / (lengths(c).toDouble + dictionarySize))
#
#     # /**
#     #  * @param c класс
#     #  * @return логарифм априорной вероятности класса <code>P(c)</code>
#     #  */
#     def classLogProbability(c: String) = log(docCount(c).toDouble / docCount.values.reduceLeft(_ + _))
#
#     # /**
#     #  * @return множество всех классов
#     #  */
#     def classes = docCount.keySet
# }
#
#
import decimal

from django.db.models import Count, Sum
from apps.nbc.process import NBCModel, normalization, save_document, Classifier

from math import log, exp

# with open('test/result.html', 'wb') as output_file:
#     output_file.write(data.encode('utf8'))

document = "надо купить сигареты"


"""

import time
import requests
from bs4 import BeautifulSoup
from gmproject.settings import logger

t = Timer()
t.run()

logger.info("Parse Cyberleninka | Start")
logger.info("Parse Cyberleninka | Получение категорий")

MAIN_URL = 'https://cyberleninka.ru/'


# Загрузить данные с url
def load_data(url):
    session = requests.Session()
    request = session.get(url)
    return request.text


# Получаем список рубрик и их url'ы
data = load_data(MAIN_URL)
soup = BeautifulSoup(data, 'html.parser')

rubrics = {}
# Ищем список с рубриками на html странице
rubric_list = soup.find('div', {'class': 'half-right'})
items = rubric_list.find_all('a')
# Составялем словарь {"Рубрика": "url"}
for item in items:
    rubric_link = item.get('href')
    rubric_name = item.text
    rubrics[rubric_name] = rubric_link

logger.info("Parse Cyberleninka | Категории получены. Прошло %s" % t.check)

test = {}
# Цикл по рубрикам
for rubric in rubrics:

    if rubric not in ["География", "Государство и право",
                      "Машиностроение", "Вычислительная техника",
                      "Информатика", "История. Исторические науки",
                      "Математика", "Химия", "Физика", "Биология"]:
        continue

    test[rubric] = 0

    category, created = Categories.objects.get_or_create(category=rubric)

    logger.info("Parse Cyberleninka | Получение статей %s " % rubric)

    # Проходим по всем статьям данной рубрики
    page = 6
    while True:

        logger.info("Parse Cyberleninka | %s, page: %s " % (rubric, page))

        articles = {}
        # Получаем статьи с данной страницы
        data = load_data(MAIN_URL + rubrics[rubric] + "/" + str(page))
        soup = BeautifulSoup(data, 'html.parser')
        articles_list = soup.find('ul', {'class': 'list'})

        # Если статьи кончились - выходим
        if not articles_list.find("li") or page > 6:
            logger.info("Parse Cyberleninka | %s, Статьи получены. Прошло %s "
                        % (rubric, t.check))
            break

        # Иначе составялем словарь {"Статья": "url"}
        else:
            items = articles_list.find_all('a')
            for item in items:
                article_link = item.get('href')
                article_name = item.find("div").text
                articles[article_name] = article_link

        logger.info("Parse Cyberleninka | %s, Сохранение статей %sй страницы" % (rubric, page))

        # Получаем текст статьи
        for article in articles:
            while True:
                data = load_data(MAIN_URL + articles[article])
                soup = BeautifulSoup(data, 'html.parser').find('div', {'class': 'ocr'})
                if not soup:
                    logger.info("Parse Cyberleninka | Capture ERROR, wait..")
                    time.sleep(15)
                    logger.info("Parse Cyberleninka | Go on")
                    continue
                else:
                    break
            text = soup.get_text()

            #save_document(category.id, text)

            # Тестирование
            result = sort_dct(Classifier().classifier(text))[0][0]
            if result == rubric:
                test[rubric] += 1

        logger.info("Parse Cyberleninka | %s, %sя сохранена. Прошло %s" % (rubric, page, t.check))
        page += 1

#
print("Результат:")
print(test)

"""

class Test:

    sp = 322

    def __init__(self, d=0, c=0):
        self.param = 2
        self.param2 = d

    def __add__(self, other):
        return 3224

    __radd__ = __add__

"""
https://oauth.vk.com/authorize?  
 client_id=APP_ID&  
 scope=PERMISSIONS&  
 redirect_uri=REDIRECT_URI&  
 response_type=code& 
v=API_VERSION

https://oauth.vk.com/access_token?  
client_id=APP_ID&  
client_secret=APP_SECRET&  
code=7a6fa4dff77a228eeda56603b8f53806c883f011c40b72630bb50df056f6479e52a&  
redirect_uri=REDIRECT_URI

"""


auth_url = (
        "https://oauth.vk.com/authorize?client_id={app_id}"
        "&scope=offline&redirect_uri=http://localhost:8000/accounts/vk/login/callback/"
        "&response_type=code&v=5.74".format(app_id=6440659)
    )

# auth_url = (
#         "https://oauth.vk.com/access_token?client_id={app_id}"
#         "&client_secret=Knj75K3i4cUsNFFOeEBv"
#         "&code=b3b057f7e117aec4c9"
#         "&redirect_uri=http://localhost:8000/accounts/vk/login/callback/".format(app_id=6440659)
#     )

webbrowser.open_new_tab(auth_url)

# t = requests.get(auth_url)
# print(t.content)