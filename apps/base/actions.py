from django.views.generic.base import TemplateView


class IndexAction(TemplateView):
    template_name = "index.html"

