from django.db import models
from enumfields import EnumField

from apps.support.api import check_token
from apps.tokens.enums import ValidEnum
from apps.support.helpers import get_account_name


class Token(models.Model):
    """
    Базовые токены проекта. Используются для разных целей.
    """

    vk_id = models.IntegerField(verbose_name="ID ВК")
    name = models.CharField(verbose_name="Имя", max_length=100)
    login = models.CharField(
        verbose_name="Логин", max_length=100, null=True, blank=True)
    password = models.CharField(
        verbose_name="Пароль", max_length=100, null=True, blank=True)
    token = models.CharField(verbose_name="Токен", max_length=200)
    is_active = models.BooleanField(
        verbose_name="Активен", default=True)
    is_free = models.BooleanField(verbose_name="Свободен", default=True)
    status = EnumField(ValidEnum, max_length=1, verbose_name="Статус",
                       null=True, blank=True)
    created = models.DateTimeField(
        verbose_name="Дата создания", auto_now_add=True)
    modified = models.DateTimeField(
        verbose_name="Дата изменения", auto_now=True)

    def change_state(self):
        self.is_active = not self.is_active
        self.save_simple()

    def check_token(self):
        if check_token(self.vk_id, self.token):
            return ValidEnum.OK
        else:
            return ValidEnum.ERROR

    def refresh_status(self):
        self.status = self.check_token()
        self.save_simple()

    def refresh_name(self):
        self.name = get_account_name(self.vk_id)
        self.save_simple()

    def save_simple(self):
        return super(Token, self).save()

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.refresh_status()
        self.refresh_name()
        return super(Token, self).save()

    def __str__(self):
        return self.name

    class Meta:
        db_table = "tokens"
        verbose_name = "Токен"
        verbose_name_plural = "Токены"





