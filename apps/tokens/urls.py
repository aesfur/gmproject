from django.conf.urls import url

from apps.tokens.actions import (
    TokenListAction, TokenUpdateAction, TokenDeleteAction,
    TokenRefreshAction, TokenChangeStateAction, TokenAddAction,
    TokenCreateAction
)

urlpatterns = [
    url(r"^$", TokenListAction.as_view(), name="tokens_list"),
    url(r"^add$", TokenAddAction.as_view(), name="token_add"),
    url(r"^create$", TokenCreateAction.as_view(), name="token_create"),
    url(r"^refresh$", TokenRefreshAction.as_view(), name="tokens_refresh"),
    url(r"^(?P<token_id>\d+)$", TokenUpdateAction.as_view(),
        name="token_update"),
    url(r"^delete/(?P<token_id>\d+)$", TokenDeleteAction.as_view(),
        name="token_delete"),
    url(r"^change/(?P<token_id>\d+)$", TokenChangeStateAction.as_view(),
        name="token_change"),
]

