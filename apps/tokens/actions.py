from django.urls import reverse_lazy
from django.shortcuts import redirect
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from django.views.generic.edit import UpdateView, DeleteView, CreateView
from django.views.generic.base import View
from django.views.generic.list import ListView

from apps.tokens.models import Token
from apps.tokens.forms import TokenCreateForm, TokenUpdateForm
from apps.support.api import get_auth_params
from apps.support.mixins import AuthenticateMixin


class TokenListAction(AuthenticateMixin, ListView):
    template_name = "tokens_list.html"
    model = Token

    def get_queryset(self):
        queryset = Token.objects.all().order_by('name')
        return queryset


class TokenCreateAction(AuthenticateMixin, SuccessMessageMixin, CreateView):
    template_name = "tokens_create.html"
    success_url = reverse_lazy("tokens_list")
    model = Token
    form_class = TokenCreateForm
    success_message = "Добавлен"


class TokenUpdateAction(AuthenticateMixin, SuccessMessageMixin, UpdateView):
    template_name = "tokens_update.html"
    success_url = reverse_lazy("tokens_list")
    pk_url_kwarg = "token_id"
    model = Token
    form_class = TokenUpdateForm
    success_message = "Токен изменен"

    def post(self, request, *args, **kwargs):
        fake = Token.objects.filter(id=self.kwargs.get(self.pk_url_kwarg))
        if not fake:
            messages.add_message(
                self.request, messages.ERROR, "Токен не существует")
            return redirect(self.success_url)
        return super(TokenUpdateAction, self).post(request, *args, **kwargs)


class TokenDeleteAction(AuthenticateMixin, DeleteView):
    model = Token
    success_url = reverse_lazy("tokens_list")
    pk_url_kwarg = "token_id"

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        fake = Token.objects.filter(id=self.kwargs.get(self.pk_url_kwarg))
        if not fake:
            messages.add_message(
                self.request, messages.ERROR, "Токен не существует")
            return redirect(self.success_url)
        else:
            messages.add_message(request, messages.SUCCESS, "Токен удален")
        return super(TokenDeleteAction, self).post(request, *args, **kwargs)


class TokenAddAction(AuthenticateMixin, View):

    def get(self, request):
        token, vk_id = get_auth_params()
        Token.objects.update_or_create(
            vk_id=vk_id,
            defaults={
                "token": token,
                "is_active": True
            })
        messages.add_message(request, messages.SUCCESS, "Токен подключен")
        return redirect(reverse_lazy("tokens_list"))


class TokenChangeStateAction(AuthenticateMixin, View):

    def get(self, request, *args, **kwargs):
        token_id = kwargs.get("token_id")
        token = Token.objects.get(id=token_id)
        token.change_state()
        return redirect(reverse_lazy("tokens_list"))


class TokenRefreshAction(AuthenticateMixin, View):

    def get(self, request):
        tokens = Token.objects.all()
        for token in tokens:
            token.refresh_name()
            token.refresh_status()
        messages.add_message(request, messages.SUCCESS, "Токены обновлены")
        return redirect(reverse_lazy("tokens_list"))







