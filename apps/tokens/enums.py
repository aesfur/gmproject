from enumfields import Enum


class ValidEnum(Enum):
    """
    Состояния аккаунта
    """
    OK = '1'
    ERROR = '0'

    class Labels:
        OK = 'Валиден'
        ERROR = 'Ошибка подключения'
