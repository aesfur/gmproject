from django.contrib import admin
from django.utils.html import format_html
from enumfields.admin import EnumFieldListFilter
from apps.tokens.models import Token
from apps.tokens.enums import ValidEnum


class TokenAdmin(admin.ModelAdmin):
    exclude = ["is_free", "status"]
    list_display = ["go_vk", "is_active", "name", "format_status", "login", "password"]
    list_editable = ["is_active"]
    list_filter = [("status", EnumFieldListFilter), "is_active"]
    search_fields = ["name", "vk_id"]
    list_display_links = ["name"]
    actions = ["refresh_status", "refresh_name"]

    def format_status(self, obj):
        if obj.status == ValidEnum.OK:
            icon = "/static/admin/img/icon-yes.svg"
        else:
            icon = "/static/admin/img/icon-no.svg"
        return format_html("<img src='%s'> %s" % (
                "%s" % icon, obj.status))
    format_status.short_description = "Статус"

    def refresh_status(self, request, queryset):
        for item in queryset:
            item.refresh_status()
        self.message_user(request, "Статусы обновлены")
    refresh_status.short_description = "Обновить статус"

    def refresh_name(self, request, queryset):
        for item in queryset:
            item.refresh_name()
        self.message_user(request, "Имена обновлены")
    refresh_name.short_description = "Обновить имя"

    def go_vk(self, obj):
        return format_html("<a href='https://vk.com/id%s'>%s</a>" %
                           (obj.vk_id, obj.vk_id))
    go_vk.short_description = "ВК ID"


admin.site.register(Token, TokenAdmin)

