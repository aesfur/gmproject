from django import forms
from apps.tokens.models import Token


class TokenUpdateForm(forms.ModelForm):
    """
    Форма изменения токена
    """

    class Meta:
        model = Token
        fields = ('login', 'password')


class TokenCreateForm(forms.ModelForm):
    """
    Форма создания токена
    """

    class Meta:
        model = Token
        fields = ('vk_id', 'token', 'login', 'password')