from django.test import TestCase
from django.contrib.auth.models import User
from apps.configuration.fakes.models import LikeFake
from apps.base.enums import ValidEnum
from django.core.urlresolvers import reverse_lazy


class LikeFakeMethodTests(TestCase):

    def setUp(self):
        self.user = User.objects.create(username="test_user1")

    def test_change_state(self):
        param = {
            "user": self.user,
            "vk_id": "1488",
            "token": "1488",
            "name": "Fail"
        }
        fake = LikeFake(**param)
        fake.change_state()
        self.assertEqual(fake.is_active, False)

    def test_refresh_name(self):
        param = {
            "user": self.user,
            "vk_id": "55444080",
            "token": "bd20ab31c85139be93b8f680a8ac113ea273be0c29"
                     "e2fe15dc1c33f675b5c7f079d5c0ca90d2b9ecb26da",
            "name": "OK"
        }
        fake = LikeFake(**param)
        fake.refresh_name()
        self.assertNotEquals(fake.name, "OK")

    def test_refresh_status(self):
        param = {
            "user": self.user,
            "vk_id": "1488",
            "token": "1488",
            "name": "Fail",
            "status": ValidEnum.OK
        }
        fake = LikeFake(**param)
        fake.refresh_status()
        self.assertEqual(fake.status, ValidEnum.ERROR)

    def test_check_token_error(self):
        param = {
            "user": self.user,
            "vk_id": "1488",
            "token": "1488",
            "name": "Fail"
        }
        fake = LikeFake(**param)
        self.assertEqual(fake.check_token(), ValidEnum.ERROR)

    def test_check_token_ok(self):
        param = {
            "user": self.user,
            "vk_id": "55444080",
            "token": "bd20ab31c85139be93b8f680a8ac113ea273be0c29"
                     "e2fe15dc1c33f675b5c7f079d5c0ca90d2b9ecb26da",
            "name": "OK"
        }
        fake = LikeFake(**param)
        self.assertEqual(fake.check_token(), ValidEnum.OK)


class LikeFakeActionTests(TestCase):

    def setUp(self):
        self.user = User.objects.create(username="test_1", password="secret")
        self.client.force_login(user=self.user)
        self.param = {
            "user": self.user,
            "vk_id": "1488",
            "token": "1488",
            "name": "Fail",
        }
        self.fake = LikeFake.objects.create(**self.param)

    def test_fake_refresh(self):
        self.fake.status = ValidEnum.OK
        self.fake.save_simple()
        response = self.client.get(reverse_lazy("fakes_refresh"))
        self.assertEqual(
            LikeFake.objects.get(id=self.fake.id).status, ValidEnum.ERROR)
        self.assertEqual(response.status_code, 302)

    def test_fake_list(self):
        response = self.client.get(reverse_lazy("fakes_list"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "fakes_list.html")

    def test_fake_create(self):
        self.param["vk_id"] = "1337"
        param = {
            "user": self.user,
            "vk_id": "1337",
            "token": "1488",
            "name": "Fail",
        }
        response = self.client.post(reverse_lazy("fake_create"), param)
        self.assertEqual(LikeFake.objects.all().count(), 2)
        self.assertEqual(response.status_code, 302)

    def test_fake_update(self):
        self.param["login"] = "1488"
        response = self.client.post(reverse_lazy(
            "fake_update", kwargs={"fake_id": self.fake.id}), self.param)
        self.assertEqual(LikeFake.objects.get(id=self.fake.login).ip, "1488")
        self.assertEqual(response.status_code, 302)

    def test_fake_delete(self):
        response = self.client.post(reverse_lazy(
            "fake_delete", kwargs={"fake_id": self.fake.id}))
        self.assertEqual(LikeFake.objects.filter(id=self.fake.id).count(), 0)
        self.assertEqual(response.status_code, 302)
