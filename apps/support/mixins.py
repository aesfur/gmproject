import time
import requests

from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.base import View
from django.urls import reverse_lazy
from requests.exceptions import ReadTimeout
from vk.exceptions import VkAPIError

from gmproject.settings import logger
from apps.support.api import get_token_api, check_token
from apps.support.helpers import Timer, valid_key, valid_index
from apps.tokens.models import Token


class AuthenticateMixin(LoginRequiredMixin, View):
    login_url = reverse_lazy('login')


class RequestMixin:

    TIME_OUT = 0.4

    def valid_request(self, passed_time):
        if passed_time < self.TIME_OUT:
            delta = self.TIME_OUT - passed_time
            logger.info('Sleep | %s' % round(delta, 4))
            time.sleep(delta)

    def execute(self, code, token):
        """
        Выполняет метод vk api - execute, обработка ошибок
        :param code: код на выполнение в api
        :param token: токен
        :return: результат запроса, код ошибки. 0 - ошибок нет.
        """

        result = []
        error = 0
        api = get_token_api(token)

        try:
            result = api.execute(code=code)

        except VkAPIError as err:
            logger.warning('VkAPIError | Code=%s' % err.code)
            logger.exception(err)
            error = err.code

        except ReadTimeout as err:
            logger.warning('ReadTimeout')
            error = 2

        except requests.exceptions.ConnectionError:
            logger.warning('get_wall ConnectionError')
            error = 6

        return result, error


class TokenMixin:
    """
    Получает из БД токены и проверяет на валидностью
    Возвращает токены с возможностью итерации по ним
    """

    def __init__(self, count=None):
        logger.info('Start | process=TokenMixin')
        self.active_token = 0
        self.count = count
        self.check_token()
        self.tokens = self.get_tokens()

    def free_token(self):
        for token in self.tokens:
            token.is_free = True
            token.save()

    def check_token(self):
        logger.info('Start | process=check_token')
        tokens = Token.objects.filter(is_active=True, is_free=True)
        for token in tokens:
            check = check_token(token.vk_id, token.token)
            if not check:
                token.is_active = False
                token.save()

    def get_tokens(self):
        logger.info('Start | process=get_tokens')
        tokens = Token.objects.filter(is_active=True, is_free=True)[:self.count]
        if not tokens:
            return []
        for token in tokens:
            token.is_free = False
            token.save()
        return [token for token in tokens]

    @property
    def current_token(self):
        """
        Возвращает текущий токен
        """
        token = valid_index(self.tokens, self.active_token)
        if token:
            return token.token
        else:
            return None

    @property
    def next_token(self):
        """
        Возвращает следующий токен
        """
        next_token = self.active_token + 1
        self.active_token = next_token if (
            valid_index(self.tokens, next_token)) else 0
        return self.current_token