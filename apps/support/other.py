

def get_data(self, user_id):
    code = """
    var user_id = {0};
    var data = [];
    data.push(API.groups.get({{
    "user_id": user_id, "extended": 1, "fields":"description","v": "5.62",
    "count": {1}}}));
    data.push(API.wall.get({{
    "owner_id": user_id, "v": "5.62", "count": 100}}));
    data.push(API.users.get({{
    "user_ids": user_id, "v": "5.62",
    "fields": "interests,about,sex,bdate"}}));
    return data;
    """.format(user_id, self.TOP_COUNT_GROUP)

    data, error = self.execute(code, self.user_token)
    if not data[0] and not data[1]:
        error = 18
    user_groups = data[0]['items'] if data[0] else []
    user_wall = data[1]['items'] if data[0] else []
    user_page = data[2][0]
    return user_groups, user_wall, user_page, error


from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse
from django.shortcuts import redirect
from django.views.generic.base import TemplateView

from apps.configuration.rubrics.models import Rubric
from apps.support.mixins import AuthenticateMixin


class RubricsAction(AuthenticateMixin, TemplateView):
    template_name = "rubrics.html"

    def get_context_data(self, **kwargs):
        context = super(RubricsAction, self).get_context_data(**kwargs)
        rubrics = Rubric.objects.all()
        context["rubrics"] = rubrics
        return context

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            if self.request.POST.get('request_type') == 'delete_rubric':
                return self.delete_rubric()
        else:
            return self.add_rubrics()

    def delete_rubric(self):
        rubric_id = self.request.POST.get('rubric_id')
        Rubric.objects.get(id=rubric_id).delete()
        return HttpResponse(200)

    def add_rubrics(self):
        rubric_name = self.request.POST.get('rubric_name')
        Rubric.objects.create(name=rubric_name)
        return redirect(reverse_lazy("rubrics"))


import json

from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse
from django.shortcuts import redirect
from django.views.generic.base import TemplateView

from apps.configuration.rubrics.models import Rubric
from apps.configuration.rubrics.models import RubricSource
from apps.configuration.sources.models import Source
from apps.support.helpers import get_group_name
from apps.support.mixins import AuthenticateMixin


class SourcesAction(AuthenticateMixin, TemplateView):
    template_name = "sources.html"

    def get_context_data(self, **kwargs):
        context = super(SourcesAction, self).get_context_data(**kwargs)
        sources = RubricSource.objects.filter(rubric__id=kwargs['rubric_id'])
        rubric = Rubric.objects.get(id=kwargs['rubric_id'])
        sources_all = Source.objects.exclude(
            source_id__in=sources.values_list("source__source_id", flat=True))
        context["rubric"] = rubric
        context["sources"] = sources
        context["sources_all"] = sources_all
        return context

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            if self.request.POST.get('request_type') == 'add_sources':
                return self.add_sources()
            if self.request.POST.get('request_type') == 'delete_source':
                return self.unhook_source()
        else:
            return self.add_source()

    def unhook_source(self):
        source_id = self.request.POST.get('source_id')
        rubric_id = self.request.POST.get('rubric_id')
        RubricSource.objects.get(
            rubric_id=rubric_id, source__source_id=source_id).delete()
        return HttpResponse(200)

    def add_sources(self):
        sources = json.loads(self.request.POST.get('rubric_source'))
        for source in sources:
            RubricSource.objects.update_or_create(
                rubric=Rubric.objects.get(id=sources[source]),
                source=Source.objects.get(source_id=source)
            )
        return HttpResponse(200)

    def add_source(self):
        source_id = self.request.POST.get('source_id')
        source_name = get_group_name(source_id)
        Source.objects.update_or_create(
            source_id=source_id, defaults={"name": source_name})
        return redirect(reverse_lazy("sources", kwargs=self.kwargs))

"""

$(function () {
    $.ajaxSetup({
        headers: { "X-CSRFToken": getCookie("csrftoken") }
    });
});

$(document).ready(function() {
$(".delete_fake").click(function() {
    var fake_id = $(this).attr('id');
    $.ajax({
        type: 'POST',
        url: '',
        data: {'request_type': 'delete_fake', 'fake_id': fake_id},
        success: function (data) {
            $( '#row' + fake_id ).remove();
            if ($('#table-fake tr').length == 1) {
                $('#table-fake').remove();
                $("#block-fake").html("<p>Нет фейков!</p>");
            }
        }
    });
    return false;
})});

$(document).ready(function() {
$(".refresh_fake").click(function() {
    var fake_id = $(this).attr('id');
    $.ajax({
        type: 'POST',
        url: '',
        data: {'request_type': 'refresh_fake', 'fake_id': fake_id},
        success: function (data) {
            var userData = JSON.parse(data);
            $("#name" + fake_id).html(userData.name);
            $("#status" + fake_id).html(userData.status);
            $("#is_free" + fake_id).html(userData.is_free);
        }
    });
    return false;
})});

$(document).ready(function() {
$(".start_fake, .pause_fake").click(function() {
    var fake_id = $(this).attr('id');
    $.ajax({
        type: 'POST',
        url: '',
        data: {'request_type': 'start_pause_fake', 'fake_id': fake_id},
        success: function (data) {
            window.location.reload()
        }
    });
    return false;
})});
"""


#### мультипроц получения даты

def get_html(url):
    response = requests.get(url)
    return response.text


def get_all_links():

    token = "c9f806bf0517fd1ccd8efebdfdb968759b59de30c5e" \
            "220e7855b3c70ea323dd085ecf70a92c99d949e34d"
    api = get_api_by_token(token)

    ids = get_friend_ids(55444080, api)

    links = []
    for id in ids:
        link = "https://vk.com/foaf.php?id={id}".format(id=id)
        links.append(link)
    return links


def get_page_data(html):

    soup = BeautifulSoup(html, "html.parser")
    created = soup.find('ya:created')
    # Если не найдена, проверяем соседний
    if not created:
        return None
    # Срезаем дату
    else:
        register_date = datetime.strptime(
            created.attrs["dc:date"].split("T")[0], "%Y-%m-%d")
        return register_date.date()



def make_all(link):
    html = get_html(link)
    data = get_page_data(html)
    return data


def main():
    t = Timer()
    t.run()

    all_links = get_all_links()

    with Pool(15) as p:
        result = p.map(make_all, all_links)

    print(t.check)
    print(result)