import time
import datetime
import requests

from collections import OrderedDict
from vk.exceptions import VkAPIError
from requests.exceptions import ProxyError

from apps.support.api import get_api, check_token
# from apps.users.enums import (
#     RelationEnum, PeopleMain, PoliticalEnum,
#     LifeMainEnums, SmokingEnum, AlcoholEnum
#  )

# ENUMS = {
#     'relation': RelationEnum, 'political': PoliticalEnum,
#     'people_main': PeopleMain, 'life_main': LifeMainEnums,
#     'smoking': SmokingEnum, 'alcohol': AlcoholEnum
# }



class GetBaseToken:
    """
    Дает уникальный доступ к базовым токенам с возможностью итерации по ним.
    """

    def __init__(self, count=None):
        self.active_token = 0
        self.count = count
        self.tokens = self.get_tokens()

    def free(self):
        for token in self.tokens:
            token.is_free = True
            token.save()

    def get_tokens(self):
        from apps.configuration.fakes.models import BaseFake
        tokens = BaseFake.objects.filter(
            is_active=True, is_alive=True, is_free=True, is_deleted=False)
        for token in tokens:
            check = check_token(token.vk_id, token.token)
            if not check:
                token.is_alive = False
                token.save()
        tokens = BaseFake.objects.filter(
            is_active=True, is_alive=True, is_free=True)[:self.count]
        if not tokens:
            return []
        for token in tokens:
            token.is_free = False
            token.save()
        return [token for token in tokens]

    @property
    def count_token(self):
        return len(self.tokens)

    @property
    def current(self):
        """
        Возвращает текущий токен
        """
        valid = valid_index(self.tokens, self.active_token)
        if valid:
            return valid.token
        else:
            return None

    @property
    def next(self):
        """
        Возвращает следующий токен
        """
        next_token = self.active_token + 1
        self.active_token = next_token if (
            valid_index(self.tokens, next_token)) else 0
        return self.current


class Timer:

    def __init__(self):
        self.start = time.time()
        self.freeze = self.start

    def pause(self):
        self.freeze = self.check

    def run(self):
        self.start = self.freeze

    @property
    def check(self):
        return round(time.time() - self.start, 4)


def valid_index(array, index):
    try:
        result = array[index]
    except IndexError:
        result = 0
    return result


def valid_key_enum(dct, key, inserted='', rtn=None):
    return valid_enum(valid_key(dct, key, inserted, rtn), key)


def valid_enum(value, key):
    try:
        valid_list = [enum for enum in ENUMS[key]
                      if isinstance(enum.value, tuple)]
        for enum in valid_list:
            if enum.value.number == value:
                return value
    except KeyError:
        return value
    return None


def valid_key(dct, key, inserted='', rtn=None):
    # TODO удалить!
    """
    Проверяет ключ на существование и возвращает его значение в случаи успеха,
    иначе значение переменной rtn
    :param dct: Словарь
    :param key: Ключ
    :param inserted: Вложенный ключ, указывается при необходимости
    :param rtn: Значение, которое следует вернуть, если ключ не существует
    :return: Значение ключа key
    """
    try:
        if inserted:
            value = dct[key][inserted]
        else:
            value = dct[key]
    except KeyError:
        value = rtn
    return value


def sort_dct(dct):
    """
    Сортирует словарь по значению
    :param dct: Словарь
    :return: Отсортированный список кортежей
    """
    return sorted(dct.items(), key=lambda x: x[1], reverse=True)


def sort_dct_key(dct):
    """
    Сортирует словарь по возрастанию ключа
    :param dct: Словарь
    :return: Осортированный словарь по ключу
    """
    return OrderedDict(sorted(dct.items(), key=lambda t: t[0]))


def annotate_city(members):
    """
    Аннотация участников сообщества по городам
    :param members: Список участников сообщества
    :return: Список: id, количество участников - [(1, 12202), (1192, 12813)]
    """
    cities = {}
    for member in members:
        cities[valid_key(member, 'city', inserted='id', rtn=0)] = (
            valid_key(cities, valid_key(
                member, 'city', inserted='id', rtn=0), rtn=0) + 1)
    return sort_dct(cities)


def get_account_name(vk_id):
    try:
        api = get_api()
        user = api.users.get(user_ids=vk_id, v="5.62")
        first_name = user[0]["first_name"]
        last_name = user[0]["last_name"]
    except VkAPIError as err:
        return err.code
    return first_name + ' ' + last_name


def get_group_name(group_id):
    try:
        api = get_api()
        group = api.groups.getById(group_id=group_id, v="5.62")
        name = group[0]["name"]
    except VkAPIError as err:
        return err.code
    return name


def check_source(source_id):
    try:
        get_api().wall.get(
            owner_id='-%s' % source_id, count=1, v="5.62"
        )['count']
    except VkAPIError as err:
        if err.code == 15:
            return False
        else:
            return check_source(source_id)
    return True


def vkdate_to_date(vkdate):
    try:
        if vkdate and len(vkdate) >= 8:
            date = datetime.datetime.strptime(vkdate, "%d.%m.%Y").date()
            return date
    except:
        return None
    return None
