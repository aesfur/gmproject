import vk
import vk_api
import webbrowser

from urllib.parse import parse_qs
from gmproject.settings import VK_APP_ID, SERVICE_ACCESS_KEY
from vk.exceptions import VkAPIError
from vk_api.exceptions import VkApiError


def check_response(response):
    """
    Проверка ответа на ошибку
    :param response:
    :return:
    """
    ERROR = False
    OK = True
    if isinstance(response, VkApiError):
        return ERROR
    return OK


def get_api_by_token(token):
    """
    Получаем api по токену
    :param token: токен пользователя
    :return: api для запросов
    """
    return vk_api.VkApi(token=token, api_version='5.74').get_api()


def vk_api_err_decorator(func_to_decorate):
    """
    Обработка ошибок во время запросов в vk api
    :param func_to_decorate: оборачиваемая функция
    """
    def wrapper(*args):
        try:
            return func_to_decorate(*args)
        except VkApiError as error:
            return error
    return wrapper



#############################################


def get_auth_params():
    auth_url = (
        "https://oauth.vk.com/authorize?client_id={app_id}"
        "&scope=market,email,stats,notifications,docs,ads,notes,status,"
        "pages,video,photos,notify,offline,wall,messages,audio,friends,"
        "groups&redirect_uri=http://oauth.vk.com/blank.html&display="
        "page&response_type=token".format(app_id=VK_APP_ID)
    )
    webbrowser.open_new_tab(auth_url)
    redirected_url = input("Paste here url you were redirected:\n")
    aup = parse_qs(redirected_url)
    aup['access_token'] = aup.pop(
        'https://oauth.vk.com/blank.html#access_token')

    return aup['access_token'][0], aup['user_id'][0]


def get_api():
    return vk.API(vk.Session(access_token=SERVICE_ACCESS_KEY), timeout=60)


def get_token_api(token):
    session = vk.Session(access_token=token)
    return vk.API(session, timeout=60)


def check_token(vk_id, token):
    try:
        get_token_api(token).groups.get(
            user_id=vk_id, v="5.62")
    except VkAPIError as err:
        if err.code == 15 or err.code == 5:
            return False
        else:
            return check_token(vk_id, token)
    return True







