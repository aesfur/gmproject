from django.conf.urls import url

from apps.check_fake.actions import CheckFakeAction


urlpatterns = [
    url(r"^$", CheckFakeAction.as_view(), name="check_fake"),
]

