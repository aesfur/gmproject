import requests
import vk_api
import datetime
import time

import threading
from queue import Queue


from multiprocessing import Pool

from vk_api.exceptions import VkApiError
from apps.support.api import get_api_by_token, vk_api_err_decorator, check_response
from apps.support.helpers import Timer
from bs4 import BeautifulSoup

"""
415531454
372633375
419874395
416360716
392791151
248903475
259598019
"""

TOKEN = "c9f806bf0517fd1ccd8efebdfdb968759b59de30c5e" \
        "220e7855b3c70ea323dd085ecf70a92c99d949e34d"

TOKEN2 = "571f35026a937dd1816c3f55dbec53f3c235f396a02a6286a0ea8a29e1f97e721bfe050dd0926794999d7"


def split_list(lst, n):
    return [lst[i:i + n] for i in range(0, len(lst), n)]


def check_friends(user):
    # Считает среднее количество общих друзей

    token = "c9f806bf0517fd1ccd8efebdfdb968759b59de30c5e" \
            "220e7855b3c70ea323dd085ecf70a92c99d949e34d"
    api = vk_api.VkApi(token=token, api_version='5.63').get_api()
    all_friends = api.friends.get(user_id=user, fields='deactivated')['items']

    valid = []
    ban = 0
    for friend in all_friends:
        if friend.get('deactivated', 0):
            ban += 1
            continue
        valid.append(friend['id'])

    friends_list = split_list(valid, 100)

    result = []
    for friends in friends_list:
        result += api.friends.getMutual(
            source_uid=user, target_uids=str(friends)[1:-1])

    common = 0
    for i in result:
        common += i['common_count']

    total_count = len(all_friends)
    average_common = common / (total_count - ban)
    percentage_ban = (ban * 100) / total_count

    return average_common, percentage_ban


# print(check_friends(419874395))

from datetime import datetime


# token = "c9f806bf0517fd1ccd8efebdfdb968759b59de30c5e" \
#         "220e7855b3c70ea323dd085ecf70a92c99d949e34d"
# api = vk_api.VkApi(token=token, api_version='5.63').get_api()
# photos = api.photos.getAll(owner_id=48827740, rev=0, count=200, extended=1)['items']
#
# photos.reverse()
#
# result = {}
# for photo in photos:
#     date = datetime.fromtimestamp(photo['date'])
#     if result.get(date, 0):
#         result[date] += 1
#     else:
#         result[date] = 1
#
# for i in result:
#     print(i, result[i])


def get_graph(user, token=TOKEN):
    """
    Строит граф связанности друзей между собой
    :param user: id пользователя
    :param token: токен, от которого будет запрос
    :return: граф, формата {"nodes": [{"id": id, "group": group*}],
                            "links": [{"source": id, "target": id]}]}
    * - ban, друг в бане; main, проверяемый пользователь; friend, обычный друг
    """
    t = Timer()
    t.run()

    graph = {"nodes": [], "links": []}
    api = vk_api.VkApi(token=TOKEN, api_version='5.64').get_api()

    def get_valid_ids(friends):
        # Удаляет из всех друзей забаненых
        valid = []
        for friend in friends:
            if friend.get('deactivated', 0):
                continue
            valid.append(friend['id'])
        return valid

    def get_friends(list_ids):
        # execute method api - получает список всех друзей по списку list_ids
        with vk_api.VkRequestsPool(api) as pool:
            friends = pool.method_one_param(
                'friends.get',  # Метод
                key='user_id',  # Изменяющийся параметр
                values=list_ids,
                # Параметры, которые будут в каждом запросе
                default_values={'fields': 'deactivated'}
            )
        return friends.result

    friends_items = get_friends([user])[user]["items"]
    valid_friends = {}

    # Смотрим друзей user
    for friend in friends_items:
        # Добавляем на граф заблокированных друзей
        if friend.get('deactivated', 0):
            graph["nodes"].append({
                "id": friend["id"],
                "name": friend["first_name"] + friend["last_name"],
                "group": "ban"
            })
            graph["links"].append(
                {"source": user, "target": friend["id"]})
        # Из остальных формируем словарь {ID пользователя: "Имя"}
        else:
            valid_friends[friend["id"]] = "{0} {1}".format(
                friend["first_name"], friend["last_name"])

    friends_ids_list = list(valid_friends.keys())
    # Словарь пользователей, у каждого два атрибута - имя и список id друзкей
    # {id : {"name": "Имя пользователя", "ids": [Список id друзей]}}
    result = {user: {"name": "Current", "ids": friends_ids_list}}

    # Получаем друзей друзей user'a
    friends_of_friends = get_friends(friends_ids_list)

    # Добавляем данные в result
    for friend_id in valid_friends:
        ids = get_valid_ids(friends_of_friends[friend_id]["items"])
        result[friend_id] = {"name": valid_friends[friend_id], "ids": ids}

    # Формуруем связи между всеми узлами
    for user_id in result:
        group = "friend"
        if user_id == user:
            group = "main"
        graph["nodes"].append(
            {"id": user_id, "group": group, "name": result[user_id]["name"]})
        for j in result[user_id]["ids"]:
            # Оставляем только друзей, которые есть в друзьях у user
            if user_id != j and j in friends_ids_list:
                graph["links"].append({"source": user_id, "target": j})

    return graph


@vk_api_err_decorator
def get_base_information(user, api):
    user = api.users.get(user_ids=user, fields="photo_200_orig")[0]
    return user


def get_domain_by_url(url):
    """
    Получить домен пользователя из url
    :param url: url
    :return: домен
    """
    return url.split("/")[-1]


def get_register_date(user_id):
    """
    Получить дату регистрации пользователя,
    если блокированный, смотрим соседних
    :param user_id: id пользователя
    :return: date
    """

    url = "https://vk.com/foaf.php?id={id}".format(id=user_id)

    # Загрузить данные с url
    session = requests.Session()
    t = Timer()
    t.run()
    request = session.get(url)
    data = request.text

    # Парсим
    soup = BeautifulSoup(data, "html.parser")
    created = soup.find('ya:created')
    # Если не найдена, проверяем соседний
    if not created:
        get_register_date(int(user_id) + 1)
    # Срезаем дату
    else:
        register_date = datetime.strptime(
            created.attrs["dc:date"].split("T")[0], "%Y-%m-%d")
        return register_date.date()


@vk_api_err_decorator
def get_friend_ids(user_id, api):
    """
    Получает список идентификаторов друзей пользователя
    :param api: используемая api
    :param user_id: пользователя для котрого нужен список ids
    :return: список ids
    """
    return api.friends.get(user_id=user_id)['items']


class ParseRegDates:
    """
    Получение дат регистраций пользователей вк с помощью многопоточного парсера
    """

    URL = "https://vk.com/foaf.php?id={id}"

    def __call__(self, users_ids):
        self.time_total = Timer()
        self.dates = []
        self.queue = self.create_queue(users_ids)
        return self.start()

    def create_queue(self, users_ids):
        """
        Создание и наполнение очереди
        """
        queue = Queue()
        for id in users_ids:
            queue.put(id)
        return queue

    def start(self):
        """
        Запуск процесса получения дат
        :return: Список дат регистраций
        """

        while self.queue.unfinished_tasks != 0:
            if self.queue.empty() or threading.activeCount() > 15:
                continue
            threading.Thread(target=self.start_thread).start()
        self.queue.join()
        print(self.time_total.check)
        return self.dates

    def start_thread(self):
        """
        Нить, которая парсит страницу с датой регистрации пользователя
        :return:
        """

        # Получить id из очереди
        user_id = self.queue.get()
        # Сформировать url
        url = self.URL.format(id=user_id)

        # Загрузить данные с url
        session = requests.Session()
        request = session.get(url)
        data = request.text

        # Ищим дату
        soup = BeautifulSoup(data, "html.parser")
        created = soup.find('ya:created')
        # Если не найдена, проверяем соседний (отправляем в очередь)
        if not created:
            self.queue.put(int(user_id) + 1)
        # Если найдена - срезаем дату
        else:
            register_date = datetime.strptime(
                created.attrs["dc:date"].split("T")[0], "%Y-%m-%d")
            # Добить дату в результат
            self.dates.append(register_date.date())
        # Задача выполнена
        self.queue.task_done()
