
$(function () {
    $.ajaxSetup({
        headers: { "X-CSRFToken": getCookie("csrftoken") }
    });
});

var graph;

$(document).ready(function() {
$("#btn-send-fake-id").click(function() {
    var fakeID = $("#fake_id").val();
    $.ajax({
        type: 'POST',
        url: '',
        data: {'fake_id': fakeID},
        success: function (result) {
            $(".container-fake-result").css("display", "block");
            $("#friend-graph").empty();
            $(".register-dates").empty();
            data = JSON.parse(result);
            graph = data["graph"];
            profile = data["profile"];
            register_dates = data["register_dates"];
            insertBase(profile);
            friendGraph(graph, 400, 400);
            registerDates(register_dates)
        }
    });
    return false;
})});


function insertBase(profile) {
  $("#base-name").text(profile["first_name"] + " " + profile["last_name"]);
  $("#base-link-vk").text("uID: " + profile["id"]).attr("href", "https://vk.com/id" + profile["id"]);
  $("#base-ava").attr("src", profile["photo_200_orig"])
}

$(document).ready(function() {
$("#btn-big-graph").click(function() {
  $("#friend-graph").empty().css('width', '1000px').css('height', '1000px');
  friendGraph(graph, 1000, 1000)
})});


$(document).ready(function() {
$("#btn-small-graph").click(function() {
  $("#friend-graph").empty().css('width', '400px').css('height', '400px');
  friendGraph(graph, 400, 400)
})});


function friendGraph(graph, width, height) {

  $('#friend-graph').append('<svg></svg>');

  var svg = d3.select("svg").attr('width', width).attr('height', height);

  var color = d3.scaleOrdinal(d3.schemeCategory20);

  var simulation = d3.forceSimulation()
      .force("link", d3.forceLink().id(function(d) { return d.id; }))
      .force("charge", d3.forceManyBody().distanceMax(300))
      .force("center", d3.forceCenter(width / 2, height / 2));

  var link = svg.append("g")
      .attr("class", "links")
    .selectAll("line")
    .data(graph.links)
    .enter().append("line")
      .attr("stroke-width", function(d) { return Math.sqrt(d.value); });

  var node = svg.append("g")
      .attr("class", "nodes")
    .selectAll("circle")
    .data(graph.nodes)
    .enter().append("circle")
      .attr("r", 5)
      .attr("fill", function(d) { return color(d.group); })
      .call(d3.drag()
          .on("start", dragstarted)
          .on("drag", dragged)
          .on("end", dragended));

  node.append("title")
      .text(function(d) { return d.id + ": " + d.name; });

  node.attr("class", function(d) { return d.group; });

  simulation
      .nodes(graph.nodes)
      .on("tick", ticked);

  simulation.force("link")
      .links(graph.links);

  $(".ban").attr('fill', '#b4413b');
  $(".main").attr('fill', '#101420');
  $(".friend").attr('fill', '#1f77b4');

  function ticked() {
    link
        .attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });

    node
        .attr("cx", function(d) { return d.x; })
        .attr("cy", function(d) { return d.y; });
  }

  function dragstarted(d) {
    if (!d3.event.active) simulation.alphaTarget(0.3).restart();
    d.fx = d.x;
    d.fy = d.y;
  }

  function dragged(d) {
    d.fx = d3.event.x;
    d.fy = d3.event.y;
  }

  function dragended(d) {
    if (!d3.event.active) simulation.alphaTarget(0);
    d.fx = null;
    d.fy = null;
  }

}


function registerDates(dates) {

  $('.register-dates').append('<canvas id="myChart">\n' +
  '      <canvas id="chart-legend-top"></canvas>\n' +
  '    </canvas>');

  var labels = [];
  var data = [];

  for (var key in dates) {
    data.push(dates[key]);
    labels.push(key);
  }

  var ctx = document.getElementById("myChart").getContext('2d');
  var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: labels,
      datasets: [{
        label: ' Количество',
        data: data,
        backgroundColor:
        'rgba(54, 162, 235, 0.2)',
        borderColor:
        'rgba(54, 162, 235, 1)',
        borderWidth: 1
      }]
    },
    options: {
      legend: {
    	  display: false
      },
      title: {
        display: true,
        text: 'Даты регистрации',
        fontSize: 16
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }
  });

}