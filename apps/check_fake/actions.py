import json

from django.http import HttpResponse
from django.urls import reverse_lazy
from django.views.generic.base import TemplateView

from apps.support.api import check_response, get_api_by_token
from apps.support.mixins import AuthenticateMixin
from apps.check_fake.core import get_graph, get_base_information, get_domain_by_url, get_friend_ids, get_register_date, ParseRegDates
from apps.support.helpers import sort_dct_key, Timer


class CheckFakeAction(AuthenticateMixin, TemplateView):
    template_name = "check_fake.html"

    def get_context_data(self, **kwargs):
        context = super(CheckFakeAction, self).get_context_data(**kwargs)
        return context

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            result = {}

            ###
            token = "c9f806bf0517fd1ccd8efebdfdb968759b59de30c5e" \
                    "220e7855b3c70ea323dd085ecf70a92c99d949e34d"
            api = get_api_by_token(token)
            ###

            fake_id = get_domain_by_url(request.POST.get("fake_id"))

            base_information = get_base_information(fake_id, api)
            fake_id = base_information["id"]

            # Получаем список id друзей пользователя
            response = get_friend_ids(fake_id, api)
            # Если ответ корректен
            if check_response(response):
                friend_ids = response
            else:
                # TODO додумать пров на ошибки
                print(response)
                return reverse_lazy("check_fake")

            # Получаем даты регистрации по списку
            register_parser = ParseRegDates()
            dates = register_parser(friend_ids)

            # Считаем количесвтво по каждому году
            register_dates = {}
            for date in dates:
                try:
                    register_dates[date.year] += 1
                except KeyError:
                    register_dates[date.year] = 1

            result["register_dates"] = sort_dct_key(register_dates)

            result["profile"] = base_information
            result["graph"] = get_graph(fake_id)

            return HttpResponse(json.dumps(result))
