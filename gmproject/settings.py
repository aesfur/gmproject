import os
import sys
import logging


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SETTINGS_PATH = os.path.join(BASE_DIR, 'gmproject')
DATA_DIR = os.path.join(BASE_DIR, 'data')


SECRET_KEY = '=(k#9p*r7b_#=zu)jwd@$n626o%zh1#f-a%%5ew+-fbdjlk#5o'
GOOGLE_API_KEY = 'AIzaSyC2xJ4GXnZe2YMl-ZsEJWl8IK9hMYD4HJQ'
SERVICE_ACCESS_KEY = '6a1fb9cf6a1fb9cf6a4a53fd746a472d9866a1f6a1fb9cf32b4fafd526e6f24fc8d1a64'
VK_APP_ID = "5805143"


DEBUG = True
SOUTH_TESTS_MIGRATE = False
SKIP_SOUTH_TESTS = True
ALLOWED_HOSTS = []


INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'apps.support',
    'apps.base',
    'apps.get_users',
    'apps.tokens',
    'apps.authenticate',
    'apps.analysis',
    'apps.nbc',
    'apps.check_fake'
]


MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'apps.base.middleware.AjaxRedirect'
]


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')]
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]


WSGI_APPLICATION = 'gmproject.wsgi.application'


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'CONN_MAX_AGE': None,
        'NAME': 'gmdb',
        'USER': 'upa',
        'PASSWORD': 'root',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

if 'test' in sys.argv:
    DATABASES['default'] = {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:'
    }


AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


LANGUAGE_CODE = 'RU'
TIME_ZONE = 'Europe/Moscow'
USE_I18N = True
USE_L10N = True
USE_TZ = True


ROOT_URLCONF = 'gmproject.urls'
STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(SETTINGS_PATH, "../apps/base/static"),
)


logger = logging.getLogger('gmproject')
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'main': {
            'format': '[%(levelname)s] | %(asctime)s | '
                      '%(module)s %(process)d-%(thread)d | %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S'
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'main'
        },
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(BASE_DIR, "logs", "log.txt"),
            'formatter': 'main'
        },
        'filedb': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(BASE_DIR, "logs", "logdb.txt"),
            'formatter': 'main'
        },
    },
    'loggers': {
        # 'django.db.backends': {
        #     'handlers': ['filedb'],
        #     'propagate': True,
        #     'level': 'INFO',
        # },
        'django.request': {
            'handlers': ['console'],# 'file'],
            'level': 'INFO',
            'propagate': True,
        },
        'gmproject': {
            'handlers': ['console'], #'file'],
            'level': 'INFO',
        }
    }
}