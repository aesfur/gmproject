from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^data/', include('apps.analysis.urls')),
    url(r'^data/', include('apps.nbc.urls')),
    # url(r'^maps/', include('apps.maps.urls')),
    # url(r'^posts/', include('apps.posting.urls')),
    url(r'^get_users/', include('apps.get_users.urls')),
    # url(r'^liking/', include('apps.liking.urls')),
    # url(r'^search_target/', include('apps.search_target.urls')),
    url(r'^auth/', include('apps.authenticate.urls')),
    # url(r'^groups/', include('apps.configuration.groups.urls')),
    # url(r'^accounts/', include('apps.configuration.accounts.urls')),
    url(r'^tokens/', include('apps.tokens.urls')),
    # url(r'^proxy/', include('apps.configuration.proxy.urls')),
    url(r'^check_fake/', include('apps.check_fake.urls')),
    url(r'^', include('apps.base.urls'))
]

